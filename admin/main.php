<?php
/**
 * @author BAKYTO
 * @copyright 2013
 */
 
// VALUE TYPE

define('VT_ANY', -1);
define('VT_DEFAULT', 0);
define('VT_SQL', 1);
define('VT_GET', 2);
define('VT_POST', 3);
define('VT_ENV', 4);
define('VT_COOKIE', 5);
define('VT_PSQL', 6);
define('VT_PGET', 7);
define('VT_PPOST', 8);
define('VT_PENV', 9);
define('VT_PCOOKIE', 10);

// ERROR
define('_ERROR_400', -1);
define('_ERROR_REQUEST_METHOD', -2);
define('_ERROR_SERVER_CONFIG', -3);
define('_ERROR_DATABASE', -4);
define('_ERROR_INFOLOG', -5);
define('_ERROR_404', -6);
define('_ERROR_NAME_NOTFOUND', -7);
define('_TRAP99', -99);

// SYSTEM CHUNCK NAME
define('DEFAULTCHUNCKNAME', 'HTTP_CONTENT');  // ??????? HTTP, ????????? ? ????? ???? ? ??????? 
define('GUESTPAGECONTENT', 'GUEST_PAGE_CONTENT');  // ??????? ???????? ????????, ??????????? ???? GuestURL ???? 
define('DEFAULTTEMPLATENAME', 'None'); // ?????? ?? ?????????

class Chunck {
    // ????????
    var $id;
    var $name;
    var $pageId;
    var $sql;
    
    // ??????
    function PaintString($text) { 
        preg_match_all('/\[\*+[-0-9_.a-zA-Z]+\*\]/', $text, $out, PREG_OFFSET_CAPTURE);
        $start = 0;
        foreach ($out[0] as $val){
            $len = $val[1] - $start;
            $contt = substr($text, $start, $len);
            echo ($contt);
            $buff = substr($val[0], 2, strlen($val[0]) - 4);
            //echo $buff;
            $value = main_GetValueType($buff);
            $key = $value['item'];
            switch ($value['type']) {
                case VT_SQL: 
                    $ch_value = $this->sql["$key"];
                    $out_buff = null;
                    preg_match_all('/\[\*+[-0-9_.a-zA-Z]+\*\]/', $ch_value, $out_buff, PREG_OFFSET_CAPTURE);
                    if (sizeof($out_buff) > 0) {
                        $this->PaintString($ch_value);
                    }else{
                        echo ($ch_value);
                    }
                    break; 
                    
                case VT_GET:    echo ($_GET["$key"]); break; 
                case VT_POST:   echo ($_POST["$key"]); break; 
                case VT_ENV:    echo ($_SERVER["$key"]); break; 
                case VT_COOKIE: echo ($_COOKIE["$key"]); break; 
                
                case VT_PSQL:   $ch_value = $this->sql["$key"]; echo ($ch_value); break;                     
                case VT_PGET:    echo ($_GET["$key"]); break; 
                case VT_PPOST:   echo ($_POST["$key"]); break; 
                case VT_PENV:    echo ($_SERVER["$key"]); break; 
                case VT_PCOOKIE: echo ($_COOKIE["$key"]); break; 
                
                default:
                    $ch_obj = new Chunck;
                    $ch_obj->name = $buff; 
                    $ch_obj->pageId = $this->pageId; 
                    $ch_obj->Paint(); 
                    break; 
            }
            $start = $val[1] + strlen($val[0]); 
       }
       $contt = substr($text, $start); 
       echo $contt;
    }
    
    function Paint(){
        $this->id = main_GetPageChunckID($this->pageId, $this->name);
        if ($this->id < 0) {return;} //REMONT ???? ???? ?? ?????? ?? ???????? ????? ?????????? [*????*]
        //echo $this->name;
        $buff = main_ChunckValue($this->id);
        if ($buff == -1) {
            echo 'ERROR#02 id=' . $this->id; 
            exit();
        }
        $ch_type = $buff['type'];
        $ch_value = $buff['value'];
        if ($ch_type == 0) { // ??????? ?????
            if (($this->name == GUESTPAGECONTENT)||($this->name == DEFAULTCHUNCKNAME)){
                // ?????????????? ???????? ??????? ??? UNIX
                $s = $ch_value;
                $s = str_replace("\r\n", "\n", $s);
                $s = str_replace("\r", "\n", $s);
                $s = preg_replace("/\n{2,}/", "\n\n", $s);
                //-----------------------------------------
                $buff = explode("\n\n", $s, 2);
                $ch_value = $buff[1];
                $buff = explode("\n", $buff[0]);
                foreach($buff as $v){
                    header(main_ChunckReplaseInText($this->pageId, $v));
                }
            }
            $this->PaintString($ch_value);
        }elseif ($ch_type == 1){ // ????
            $res = main_ValueToBlock($ch_value); // sql, templ\
            if ($res['sql'] != "") {
				$query = main_ChunckReplaseInText($this->pageId, $res['sql']);
                $t = mysql_query($query);
                if (($t) and (mysql_num_rows($t) > 0)){
                    while ($sql = mysql_fetch_array($t)){
            			$this->sql = $sql;
                        $this->PaintString($res['templ']);
            		}
                    $this->sql = null;
            	}
			}
        }
    }
}

function main_GetChuncks($str){
    preg_match_all('/\[\*+[-0-9._a-zA-Z]+\*\]/', $str, $out, PREG_OFFSET_CAPTURE);
    $val = $out[0];
    $res = null;
    $ii = 0;
    while ($ii < count($val)){
        $res[$ii] = substr($val[$ii][0], 2, strlen($val[$ii][0]) - 4);
        $ii = $ii + 1;
    }
    return $res;
}

function main_ChunckReplaseInText($page_id, $in_str){
    $chs = main_GetChuncks($in_str);
    if  ($chs == null) return $in_str;
    $ii = 0;
    while($ii < count($chs)){
        $value = main_GetValueType($chs[$ii]);
        $key = $value['item'];
        switch ($value['type']) {
            case VT_SQL:    $buff = $chs[$ii]; break; 
            case VT_GET:    $buff = $_GET["$key"]; break; 
            case VT_POST:   $buff = $_POST["$key"]; break; 
            case VT_ENV:    $buff = $_SERVER["$key"]; break; 
            case VT_COOKIE: $buff = $_COOKIE["$key"]; break; 
            case VT_PSQL:    $buff = $chs[$ii]; break; 
            case VT_PGET:    $buff = $_GET["$key"]; break; 
            case VT_PPOST:   $buff = $_POST["$key"]; break; 
            case VT_PENV:    $buff = $_SERVER["$key"]; break; 
            case VT_PCOOKIE: $buff = $_COOKIE["$key"]; break; 
            
            default:
                (int)$chunck_id = main_GetPageChunckID($page_id, $chs[$ii]);
				if ($chunck_id < 0) {
					$buff = '[*' . $chs[$ii] . '*]';
                    //$buff = $chs[$ii];
                }else{
					$buff = main_ChunckValue($chunck_id);
                }
                break; 
        }
        $in_str = str_replace("[*". $chs[$ii] . "*]", $buff, $in_str);
        $ii = $ii + 1;
    }
    
    return $in_str;
}

function main_GetValueType($ch_name){
    $buff = null;
    $buff = explode('.', $ch_name, 2);
    
    if (($buff[0]=='') or ($buff[1]=='')){
        $res['key'] = '';
        $res['item'] = $ch_name;
        $res['type'] = VT_DEFAULT;
        return $res;
    }
    $res['key'] = $buff[0];
    $res['item'] = $buff[1];
    $res['type'] = VT_ANY;
    if (strtoupper($res['key']) == 'SQL')   $res['type'] = VT_SQL;
    if (strtoupper($res['key']) == 'POST')  $res['type'] = VT_POST;
    if (strtoupper($res['key']) == 'GET')   $res['type'] = VT_GET;
    if (strtoupper($res['key']) == 'ENV')   $res['type'] = VT_ENV;
    if (strtoupper($res['key']) == 'COOKIE') $res['type'] = VT_COOKIE;
    
    if (strtoupper($res['key']) == 'PSQL')   $res['type'] = VT_PSQL;
    if (strtoupper($res['key']) == 'PPOST')  $res['type'] = VT_PPOST;
    if (strtoupper($res['key']) == 'PGET')   $res['type'] = VT_PGET;
    if (strtoupper($res['key']) == 'PENV')   $res['type'] = VT_PENV;
    if (strtoupper($res['key']) == 'PCOOKIE') $res['type'] = VT_PCOOKIE;
    
    return $res;
}

function main_ValueToBlock ($s){
    // ?????????????? ???????? ??????? ??? UNIX
    $s = str_replace("\r\n", "\n", $s);
    $s = str_replace("\r", "\n", $s);
    $s = preg_replace("/\n{2,}/", "\n\n", $s);
    //-----------------------------------------
    
    $buff = explode("\n\n", $s, 2);
    $bsql = explode("\n", $buff[0], 2);
    $btmp = explode("\n", $buff[1], 2);
    $res['sql'] = $bsql[1];
    $res['templ'] = $btmp[1];
    return $res;
}

function test(){
    $text = "SELECT * [*FROM*] `oba$config` WHERE `key`='[*COOKIE.admin_name*]' AND `opt1`=101";
    $out = main_GetChuncks($text);
    echo $out;
}
//test();

function main_Error($errType, $msg){
    switch($errType){
		case _ERROR_400:
			echo('ERROR 400 [HTTP Error 400 Bad request]');
			break;
        case _ERROR_REQUEST_METHOD:
            echo('ERROR [INCORRECT REQUEST_METHOD OR INCORRECT SERVER CONFIGS]');
            break;
        case _ERROR_SERVER_CONFIG:
            echo('ERROR [INCORRECT SERVER CONFIGS]');
			break;
        case _ERROR_DATABASE:
            echo('ERROR [INCORRECT MAIN DATABASE]');
			break;
        case _ERROR_INFOLOG:
            echo('ERROR [INCORRECT MAIN DATABASE]');
			break;
        case _ERROR_404:
            echo('ERROR 404 [HTTP Error 404 Page not found]');
			break;
        case _TRAP99:
            echo('TRAP [#99]');
			break;
        default: 
            echo('UNKNOWN ERROR');
            break;
  	}
    echo ('  -'.$msg.'</br>');
}
function main_InFolderGetFolderID($parent_id, $foldername){
    //?????????? ID, ??? ?????? ?????????? -1
    $query = 'SELECT `id` FROM `oba$folder` WHERE `name`=\'' . $foldername . '\' AND `parent_id`=' . $parent_id;
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		return (int)$sql['id'];
	}else{
	   return _ERROR_NAME_NOTFOUND;
	}
}
function main_InFolderGetPageID($parent_id, $page_name){
    //?????????? ID ????????, ??? ?????????? ??? ??????
    $query = 'SELECT `id` FROM `oba$paget` WHERE `name`=\'' . $page_name . '\' AND `parent_id`=' . $parent_id;
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		return (int)$sql['id'];
	}else{
	   return _ERROR_NAME_NOTFOUND;
	}
}
function main_GetPageAuthorized($id){
    $query = 'SELECT `authorized` FROM `oba$paget` WHERE `id`=' . $id;
    $t = mysql_query($query);
    $res = 0;
	if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		$res = $sql['authorized'];
	}
    if ($res == 1) return true;
    return false;
}
function main_GetPageCondition($id){
    $query = 'SELECT `condition` FROM `oba$paget` WHERE `id`=' . $id;
    $t = mysql_query($query);
    $res = '';
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		$res = $sql['condition'];
	}
    return $res;
}
function main_GetPageChunckID($page_id, $ch_name){
	// ?????????? ID ?????? chunck, ??? ?????? ?????????? -1
    $query = 'SELECT `id` FROM `oba$chunck_name` WHERE `name`=\'' . $ch_name . "'";
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		$ch_id = $sql['id'];
	}
    $query = 'SELECT `id` FROM `oba$chunck` WHERE `paget_id`=' . $page_id . ' AND `chunck_name_id`=' . $ch_id;
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		$res = $sql['id'];
	}else{
	   return _ERROR_NAME_NOTFOUND;
	}
    return (int)$res;
}
function main_ChunckValue($chunck_id){
    $query = 'SELECT `chunck_val_id` FROM `oba$chunck` WHERE `id`=' . $chunck_id;
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		$gen_id = $sql['chunck_val_id'];
	}else{
        return -1; //chunck_id  ?? ??????
    }
    $query = 'SELECT `val` FROM `oba$chunck_val` WHERE `id`=' . $gen_id;
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		$value = $sql['val'];
	}else{
        return -1; //chunck.val_id  ?? ??????
    }
    $query = 'SELECT `chunck_type` FROM `oba$chunck` WHERE `id`=' . $chunck_id;
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		$type = $sql['chunck_type'];
	}else{
        return -1; //chunck_id  ?? ??????
    }
    $res['type'] = $type;
    $res['value'] = $value;
    return $res;
}
function main_GetPageGuestUrl($id){
    $query = 'SELECT `guesturl` FROM `oba$paget` WHERE `id`=' . $id;
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		return $sql['guesturl'];
	}else{
        return ''; 
    }
}
function main_GetTemplateFile ($page_id){
    $query = 'SELECT `template_name` FROM `oba$paget` WHERE `id`=' . $page_id;
    $t = mysql_query($query);
    if (($t) and (mysql_num_rows($t) > 0)){
		$sql = mysql_fetch_array($t);
		return $sql['template_name'];
	}else{
        return ''; 
    }
}

?>