<?php
    $error = '';
    if (isset($_POST['name'])&&(isset($_POST['password']))) {
        require_once "config.php";
        if (($_POST['name']==$dbuser)&&($_POST['password']==$dbpasswd)){
            $content = file_get_contents('install.sql');
            $queries = explode(';', $content);
            foreach($queries as $query){
                mysql_query($query);
            }
            header("Location: http://" . $_SERVER['HTTP_HOST'] . '/admin');
        }else{
            $error = '<p style="color: rgb(170, 17, 17);font-size: 1.2em;margin-bottom: 2px;">User name or password is not valid</p>';
        }
    }
    
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
	<head>
		<base href="<?php echo 'http://'.$_SERVER[HTTP_HOST].'/admin';?>/>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		<title>Install CIC</title>
		<meta name="description" content="">
		<link rel="stylesheet" type="text/css" href="styles/style.css">
		
	</head>
	<body>
	<div id="banner">
        <p>WELCOME TO CIC</p>
    </div>
	<div class="section box">
	  <h2>INSTALL</h2>
	  <p>Please enter user name of mysql and password</p>
	  <div class="box2">
      <?php echo $error;?>
		<div id="txt_fld11">
		  <form name="login"  method="POST" action="install.php">
			<input name="name" type="text" placeholder="User name" value="" >
			<input name="password" type="password" placeholder="Password" value="" >
			<button type="submit">  OK  </button>
		  </form>
		</div>
	  </div>
	</div>
	</body>
</html>