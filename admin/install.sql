-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 12 2014 г., 05:33
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `cicold`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oba$chunck`
--

CREATE TABLE IF NOT EXISTS `oba$chunck` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CHUNCK_NAME_ID` int(11) DEFAULT NULL,
  `CHUNCK_TYPE` smallint(6) DEFAULT NULL,
  `CHUNCK_VAL_ID` int(11) DEFAULT NULL,
  `PAGET_ID` int(11) DEFAULT NULL,
  `WBDELETE` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=253 ;

--
-- Дамп данных таблицы `oba$chunck`
--

INSERT INTO `oba$chunck` (`ID`, `CHUNCK_NAME_ID`, `CHUNCK_TYPE`, `CHUNCK_VAL_ID`, `PAGET_ID`, `WBDELETE`) VALUES
(1, 1, 0, 1, 1, 0),
(2, 2, 0, 1, 1, 0),
(3, 3, 1, 402, 1, 0),
(4, 4, 0, 1, 1, 0),
(5, 5, 1, 505, 1, 0),
(6, 6, 0, 26, 1, 0),
(7, 7, 0, 238, 1, 0),
(8, 8, 0, 7, 1, 0),
(9, 9, 1, 513, 1, 0),
(10, 10, 1, 512, 1, 0),
(11, 11, 0, 239, 1, 0),
(12, 12, 0, 240, 1, 0),
(13, 13, 1, 359, 1, 0),
(14, 14, 1, 400, 1, 0),
(15, 15, 1, 382, 1, 0),
(16, 16, 1, 356, 1, 0),
(17, 1, 0, 1, 2, 0),
(18, 2, 0, 1, 2, 0),
(19, 8, 0, 7, 2, 0),
(20, 3, 1, 77, 2, 0),
(21, 4, 0, 1, 2, 0),
(22, 5, 1, 506, 2, 0),
(23, 6, 1, 244, 2, 0),
(24, 7, 0, 274, 2, 0),
(25, 1, 0, 1, 3, 0),
(26, 2, 0, 1, 3, 0),
(27, 8, 0, 7, 3, 0),
(28, 3, 1, 76, 3, 0),
(29, 4, 0, 1, 3, 0),
(30, 5, 1, 506, 3, 0),
(31, 6, 1, 424, 3, 0),
(32, 7, 0, 274, 3, 0),
(40, 1, 0, 1, 4, 0),
(41, 2, 0, 1, 4, 0),
(42, 8, 0, 7, 4, 0),
(43, 3, 1, 75, 4, 0),
(44, 4, 0, 1, 4, 0),
(45, 5, 1, 502, 4, 0),
(46, 6, 1, 242, 4, 0),
(47, 7, 0, 274, 4, 0),
(55, 1, 0, 1, 5, 0),
(56, 2, 0, 1, 5, 0),
(57, 8, 0, 7, 5, 0),
(58, 3, 1, 74, 5, 0),
(59, 4, 0, 1, 5, 0),
(60, 5, 1, 502, 5, 0),
(61, 6, 1, 425, 5, 0),
(62, 7, 0, 274, 5, 0),
(70, 1, 0, 0, 6, 0),
(71, 2, 0, 0, 6, 0),
(72, 1, 0, 1, 7, 0),
(73, 2, 0, 1, 7, 0),
(74, 3, 1, 130, 7, 0),
(75, 4, 0, 1, 7, 0),
(76, 5, 1, 516, 7, 0),
(77, 6, 0, 26, 7, 0),
(78, 7, 0, 269, 7, 0),
(79, 8, 0, 7, 7, 0),
(80, 9, 1, 51, 7, 0),
(81, 10, 1, 458, 7, 0),
(82, 11, 0, 417, 7, 0),
(83, 12, 0, 518, 7, 0),
(84, 13, 1, 50, 7, 0),
(85, 14, 1, 450, 7, 0),
(86, 15, 1, 515, 7, 0),
(87, 16, 1, 255, 7, 0),
(103, 1, 0, 1, 8, 0),
(104, 2, 0, 1, 8, 0),
(105, 3, 0, 110, 8, 0),
(106, 4, 0, 1, 8, 0),
(107, 5, 1, 505, 8, 0),
(108, 6, 0, 442, 8, 0),
(109, 7, 0, 217, 8, 0),
(110, 8, 0, 7, 8, 0),
(116, 14, 1, 475, 8, 0),
(117, 15, 1, 443, 8, 0),
(119, 18, 1, 517, 7, 0),
(120, 19, 0, 446, 7, 0),
(121, 1, 0, 1, 9, 0),
(122, 2, 0, 1, 9, 0),
(123, 8, 0, 7, 9, 0),
(124, 3, 1, 388, 9, 0),
(125, 4, 0, 1, 9, 0),
(126, 5, 1, 496, 9, 0),
(127, 6, 1, 428, 9, 0),
(128, 7, 1, 386, 9, 0),
(137, 1, 0, 1, 10, 0),
(138, 2, 0, 1, 10, 0),
(139, 8, 0, 7, 10, 0),
(140, 3, 1, 392, 10, 0),
(141, 4, 0, 1, 10, 0),
(142, 5, 1, 498, 10, 0),
(143, 6, 1, 389, 10, 0),
(144, 7, 1, 390, 10, 0),
(152, 1, 0, 1, 11, 0),
(153, 2, 0, 1, 11, 0),
(154, 8, 0, 7, 11, 0),
(155, 3, 1, 220, 11, 0),
(156, 4, 0, 1, 11, 0),
(157, 5, 1, 498, 11, 0),
(158, 6, 1, 426, 11, 0),
(159, 7, 1, 384, 11, 0),
(160, 1, 0, 1, 12, 0),
(161, 2, 0, 1, 12, 0),
(162, 8, 0, 7, 12, 0),
(163, 3, 0, 223, 12, 0),
(164, 4, 0, 1, 12, 0),
(165, 5, 1, 505, 12, 0),
(166, 6, 0, 313, 12, 0),
(167, 7, 0, 334, 12, 0),
(168, 1, 0, 1, 13, 0),
(169, 2, 0, 1, 13, 0),
(170, 22, 0, 250, 7, 0),
(173, 22, 0, 263, 9, 0),
(174, 1, 0, 1, 14, 0),
(175, 2, 0, 1, 14, 0),
(176, 8, 0, 7, 14, 0),
(177, 3, 0, 296, 14, 0),
(178, 4, 0, 1, 14, 0),
(179, 5, 1, 509, 14, 0),
(180, 6, 0, 292, 14, 0),
(181, 7, 0, 291, 14, 0),
(182, 25, 1, 520, 14, 0),
(183, 26, 1, 521, 14, 0),
(184, 27, 1, 522, 14, 0),
(185, 28, 1, 294, 12, 0),
(186, 1, 0, 1, 15, 0),
(187, 2, 0, 1, 15, 0),
(188, 8, 0, 7, 15, 0),
(189, 3, 0, 298, 15, 0),
(190, 4, 0, 1, 15, 0),
(191, 5, 1, 501, 15, 0),
(192, 6, 0, 303, 15, 0),
(193, 7, 0, 304, 15, 0),
(194, 22, 0, 453, 15, 0),
(195, 1, 0, 1, 16, 0),
(196, 2, 0, 1, 16, 0),
(197, 8, 0, 7, 16, 0),
(198, 3, 0, 311, 16, 0),
(199, 4, 0, 1, 16, 0),
(200, 5, 1, 507, 16, 0),
(201, 6, 0, 320, 16, 0),
(202, 7, 0, 291, 16, 0),
(203, 1, 0, 1, 17, 0),
(204, 2, 0, 1, 17, 0),
(205, 8, 0, 7, 17, 0),
(206, 3, 0, 321, 17, 0),
(207, 4, 0, 1, 17, 0),
(208, 5, 0, 336, 17, 0),
(209, 6, 0, 332, 17, 0),
(210, 7, 0, 328, 17, 0),
(211, 1, 0, 473, 18, 0),
(212, 2, 0, 1, 18, 0),
(213, 1, 0, 1, 19, 0),
(214, 2, 0, 1, 19, 0),
(215, 8, 0, 7, 19, 0),
(216, 3, 0, 337, 19, 0),
(217, 4, 0, 1, 19, 0),
(218, 5, 1, 508, 19, 0),
(219, 6, 0, 523, 19, 0),
(220, 7, 0, 304, 19, 0),
(221, 22, 0, 351, 19, 0),
(222, 29, 1, 451, 7, 0),
(223, 1, 0, 1, 21, 0),
(224, 2, 0, 1, 21, 0),
(225, 8, 0, 7, 21, 0),
(226, 3, 1, 464, 21, 0),
(227, 4, 0, 1, 21, 0),
(228, 5, 1, 510, 21, 0),
(229, 6, 1, 511, 21, 0),
(230, 7, 0, 470, 21, 0),
(231, 1, 0, 474, 22, 0),
(232, 2, 0, 1, 22, 0),
(233, 30, 1, 472, 22, 0),
(234, 31, 1, 500, 9, 0),
(235, 32, 1, 487, 9, 0),
(236, 31, 1, 500, 11, 0),
(237, 32, 1, 487, 11, 0),
(238, 31, 1, 500, 10, 0),
(239, 32, 1, 487, 10, 0),
(240, 31, 1, 500, 3, 0),
(241, 31, 1, 514, 7, 0),
(242, 31, 1, 500, 2, 0),
(243, 1, 0, 1, 23, 0),
(244, 2, 0, 1, 23, 0),
(245, 1, 0, 1, 24, 0),
(246, 2, 0, 1, 24, 0),
(248, 1, 0, 1, 25, 0),
(249, 2, 0, 1, 25, 0),
(250, 1, 0, 1, 26, 0),
(251, 2, 0, 1, 26, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oba$chunck_name`
--

CREATE TABLE IF NOT EXISTS `oba$chunck_name` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=33 ;

--
-- Дамп данных таблицы `oba$chunck_name`
--

INSERT INTO `oba$chunck_name` (`ID`, `NAME`) VALUES
(1, 'HTTP_CONTENT'),
(2, 'GUEST_PAGE_CONTENT'),
(3, 'title'),
(4, 'description'),
(5, 'header'),
(6, 'content'),
(7, 'footer'),
(8, 'base_href'),
(9, 'analog_list'),
(10, 'page_list'),
(11, '_folders'),
(12, '_pages'),
(13, 'folder_list'),
(14, 'group_list'),
(15, 'folder_list_box'),
(16, 'page_list_box'),
(18, 'authorized'),
(19, 'folder_template'),
(22, '_js_css'),
(25, 'purge_names'),
(26, 'purge_values'),
(27, 'purge_all'),
(28, 'phpmyadmin_url'),
(29, 'group_list0'),
(30, 'command'),
(31, 'fn_pg_list'),
(32, 'ch_list');

-- --------------------------------------------------------

--
-- Структура таблицы `oba$chunck_val`
--

CREATE TABLE IF NOT EXISTS `oba$chunck_val` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VAL` mediumtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=524 ;

--
-- Дамп данных таблицы `oba$chunck_val`
--

INSERT INTO `oba$chunck_val` (`ID`, `VAL`) VALUES
(1, ''),
(7, 'http://[*ENV.HTTP_HOST*]/admin/'),
(26, '[*_folders*]\r[*_pages*]'),
(50, 'SQL\rSELECT `id`, `name`  FROM `oba$folder` ORDER BY `name`\r\rBLOCK\r<option value="[*SQL.id*]">[*SQL.name*]</option>'),
(51, 'SQL\rSELECT `id`, `name`  FROM `oba$paget`  ORDER BY `name`\r\rBLOCK\r<option value="[*SQL.id*]">[*SQL.name*]</option>'),
(74, 'SQL\rSELECT `name` FROM `oba$folder` WHERE `id`=0[*GET.id*]\r\rBLOCK\rDelete folder - [*SQL.name*]'),
(75, 'SQL\rSELECT `name` FROM `oba$folder` WHERE `id`=0[*GET.id*]\r\rBLOCK\rRename folder - [*SQL.name*]'),
(76, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\rDelete page - [*SQL.name*]'),
(77, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\rRename page - [*SQL.name*]'),
(110, 'Templates'),
(130, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\rEdit page - [*SQL.name*]'),
(217, '<p id="" class="section box"><a href="[*ENV.HTTP_REFERER*]"> < Go to</a> | <strong>STRUCTURE</strong></p>'),
(220, 'SQL\rSELECT cn.`name` FROM `oba$chunck_name` AS cn, `oba$chunck` AS c WHERE c.`chunck_name_id`=cn.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\rDelete container - [*SQL.name*]'),
(223, 'Administrator - [*COOKIE.admin_name*]'),
(238, '<p id="" class="section box"><a href="#"> < Go back </a> | <strong>  HOME</strong></p>'),
(239, '<div class="section box">\r  <h2>Folders\\Groups</h2>\r  <p><strong>Greate a new folder\\group</strong></p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="new_folder"  method="POST" action="siteman.php?action=new_folder">\r        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]">\r        <input name="name" type="text">\r        <button type="submit">Create</button>\r      </form>\r    </div>\r  </div>\r[*folder_list_box*]\r</div>'),
(240, '<div class="section box">\r  <h2>Pages</h2>\r  <p><strong>Greate a new page</strong></p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="new_page" method="POST" action="siteman.php?action=new_page">\r        <input name="name" type="text">\r        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]">\r        <select name="folder">\r          <option value="0" selected>select folder (root)</option>\r         [*folder_list*]\r        </select>\r        <select name="analog_page">\r          <option value="0" selected>select analog (none)</option>\r         [*analog_list*]\r        </select>\r        <button type="submit">Create</button>\r      </form>\r    </div>\r  </div>\r[*page_list_box*]\r</div>'),
(242, 'SQL\rSELECT `name` FROM `oba$folder` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Rename folder\\group</h2>\r  <p>Enter new name:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="ren_folder"  method="POST" action="siteman.php?action=ren_folder&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="new_name" type="text" value="[*SQL.name*]">\r        <button type="submit">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(244, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Rename page</h2>\r  <p>Enter new name:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="ren_page"  method="POST" action="siteman.php?action=ren_page&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="new_name" type="text" value="[*SQL.name*]">\r        <button type="submit">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(250, '<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">\r<link rel="stylesheet" href="js/codemirror/addon/display/fullscreen.css">\r\r\r<script src="js/jquery.js"></script>\r<script src="js/codemirror/lib/codemirror.js"></script>\r<script src="js/codemirror/addon/display/fullscreen.js"></script>\r<script src="js/codemirror/mode/sql.js"></script>\r\r<script src="js/colorer.js"></script>\r<script src="js/main.js"></script>'),
(255, 'SQL\rSELECT `id` FROM `oba$chunck` WHERE `paget_id`=0[*GET.id*] LIMIT 1\r\rBLOCK\r<p><strong>Containers</strong> (list):</p>\r  <div class="box2">\r    <table  width="100%" class="edit_list">\r      <tbody>\r       [*page_list*]\r      </tbody>\r    </table>\r  </div>'),
(263, '<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">\r<link rel="stylesheet" href="js/codemirror/addon/display/fullscreen.css">\r\r<script src="js/jquery.js"></script>\r<script src="js/codemirror/lib/codemirror.js"></script>\r<script src="js/codemirror/addon/display/fullscreen.js"></script>\r<script src="js/codemirror/addon/edit/closetag.js"></script>\r<script src="js/codemirror/mode/htmlmixed.js"></script>\r<script src="js/codemirror/mode/javascript.js"></script>\r<script src="js/codemirror/mode/xml.js"></script>\r<script src="js/codemirror/mode/php.js"></script>\r<script src="js/codemirror/mode/sql.js"></script>\r\r<script src="js/colorer.js"></script>\r<script src="js/container_textarea.js"></script>'),
(269, '<p id="" class="section box"><a href="../?p=cic_admin_struct"> < Go to</a> | <strong>STRUCTURE</strong></p>'),
(274, '<p id="" class="section box"><a href="../?p=cic_admin_struct"> < Go back</a> | <strong>  CANCEL</strong></p>'),
(291, '<p id="" class="section box"><a href="../?p=cic_admin_home"> < Go back</a> | <strong>  CANCEL</strong></p>'),
(292, '<div class="section box">\r  <h2>Purge tables (container names & values)</h2>\r  <p>Container names</p>[*purge_names*]\r  <p>Container values</p>[*purge_values*]\r</div>\r[*purge_all*]'),
(294, 'SQL\rSELECT `value` FROM `oba$config` WHERE `key`=''phpmyadmin_url''\r\rBLOCK\r[*SQL.value*]'),
(296, 'Purge tables'),
(298, 'File Manager'),
(303, '<div class="section box">\r  <h2>File Manager</h2>\r  <p>Create, edit, delete, rename your files</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <!-- Element where elFinder will be created (REQUIRED) -->\r      <div id="elfinder"></div>\r    </div>\r  </div>\r</div>'),
(304, '<p id="" class="section box"><a href="../?p=cic_admin_home"> < Go to</a> | <strong>  HOME</strong></p>'),
(311, 'Change login & password - [*COOKIE.admin_name*]'),
(313, '<div class="section box">\n  <h2>Home</h2>\n  <p>Main</p>\n  <div class="box2">\n    <div id="txt_fld11">\n      <table>  \n        <tbody>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_struct"><strong>Structure</strong></a></td><td> - Edit site structure</td>\n          </tr>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_filemanager"><strong>File manager</strong></a></td><td> - Create, edit, rename, delete server files on your browser</td>\n          </tr>\n          <tr>\n            <td width="180px"><a href="[*phpmyadmin_url*]"><strong>PhpMyAdmin</strong></a></td><td> - Administer database\\s width phpMyAdmin </td>\n          </tr>\n        </tbody>\n      </table>  \n    </div>\n  </div>\n  <p>Tools</p>\n  <div class="box2">\n    <div id="txt_fld11">\n      <table>  \n        <tbody>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_tool_purge"><strong>Purge</strong></a></td><td> - Clear free container names and container values</td>\n          </tr>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_tool_change"><strong>Change login/password</strong></a></td><td> - Change administrator name and password</td>\n          </tr>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_tool_mysql"><strong>MySQL statement</strong></a></td><td> - Execute mysql statements in your database</td>\n          </tr>\n        </tbody>\n      </table>  \n    </div>\n  </div>\n</div>'),
(320, '<div class="section box">\r  <h2>Change login and password</h2>\r  <p>Change login:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="change_login"  method="POST" action="siteman.php?action=change_login">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="new_name" type="text" placeholder="New login" value="" required >\r        <input name="password" type="password" placeholder="Current password" value="" required >\r        <button type="submit">  OK  </button>\r      </form>\r    </div>\r  </div>\r  <p>Change password:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="change_password"  method="POST" action="siteman.php?action=change_password">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="new_name" type="password" placeholder="New password" value="" required >\r        <input name="new_name" type="password" placeholder="Confirm password" value="" required >\r        <input name="password" type="password" placeholder="Current password" value="" required >\r        <button type="submit">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(321, 'Login to cicAdmin'),
(328, '<!-- <p id="" class="section box">&copy; <a href="http://www.obasoft.com">Obasoft</a> 2013-2014 | <strong>  CIC</strong></p> -->'),
(332, '<div class="section box">\n  <h2>Login</h2>\n  <p>Please enter user name and password</p>\n  <div class="box2">\n    <div id="txt_fld11">\n      <form name="login"  method="POST" action="siteman.php?action=login">\n        <input name="gotourl" type="hidden" value="../?p=cic_admin_home">\n        <input name="name" type="text" placeholder="User name" value="" >\n        <input name="password" type="password" placeholder="Password" value="" >\n        <button type="submit">  Login  </button>\n      </form>\n    </div>\n  </div>\n</div>'),
(334, '<p id="" class="section box"><a href="../?p=cic_admin_logout"> < Exit </a> | <strong>  LOG OUT</strong></p>'),
(336, '<div id="banner">\r    <p>WELCOME TO CIC ADMIN</p>\r</div>'),
(337, 'MySQL statement '),
(351, '<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">\r<link rel="stylesheet" href="js/codemirror/addon/display/fullscreen.css">\r\r\r<script src="js/jquery.js"></script>\r<script src="js/codemirror/lib/codemirror.js"></script>\r<script src="js/codemirror/addon/display/fullscreen.js"></script>\r<script src="js/codemirror/mode/sql.js"></script>\r\r<script src="js/colorer.js"></script>\r<script src="js/sql_result.js"></script>\r<script src="js/main.js"></script>'),
(356, 'SQL\rSELECT `id` FROM `oba$paget` WHERE `type` >= (0-0[*GET.system*])  LIMIT 1\r\rBLOCK\r<p><strong>Pages</strong> (list):</p>\r  <div class="box2">\r    <table  width="100%" class="edit_list">\r      <tbody>\r       [*page_list*]\r      </tbody>\r    </table>\r  </div>'),
(359, 'SQL\rSELECT `id`, `name`  FROM `oba$folder` WHERE `type` >= (0-0[*GET.system*]) ORDER BY `name`\r\rBLOCK\r<option value="[*SQL.id*]">[*SQL.name*]</option>'),
(382, 'SQL\r\nSELECT `id` FROM `oba$folder` WHERE `type` >= (0-0[*GET.system*])  LIMIT 1\r\n\r\nBLOCK\r\n<p><strong>Groups</strong> (list):</p>\r\n  <div class="box2">\r\n    <table  width="100%" class="edit_list">\r\n      <tbody>\r\n       [*group_list*]\r\n      </tbody>\r\n    </table>\r\n  </div>'),
(384, 'SQL\nSELECT `paget_id` FROM `oba$chunck` WHERE `ID`=0[*GET.id*]\n\nBLOCK\n<p id="" class="section box"><a href="../?p=cic_admin_p_mod&id=[*SQL.paget_id*]"> < Go back</a> | <strong>  CANCEL</strong></p>'),
(386, 'SQL\nSELECT `paget_id` FROM `oba$chunck` WHERE `ID`=0[*GET.id*]\n\nBLOCK\n<p id="" class="section box"><a href="../?p=cic_admin_p_mod&id=[*SQL.paget_id*]"> < Go back</a> | <strong> EDIT PAGE </strong></p>'),
(388, 'SQL\rSELECT n.`name` AS nn, p.`name` AS pn FROM `oba$paget` AS p, `oba$chunck_name` AS n, `oba$chunck` AS c WHERE c.`paget_id`=p.`id` AND c.`chunck_name_id`=n.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\rContainer - [*SQL.nn*] ([*SQL.pn*])'),
(389, 'SQL\rSELECT cn.`name` AS n FROM `oba$chunck_name` AS cn, `oba$chunck` AS c WHERE c.`chunck_name_id`=cn.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Rename container</h2>\r  <p>Enter new name:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="ren_container"  method="POST" action="siteman.php?action=ren_container&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="new_name" type="text" value="[*SQL.n*]">\r        <button type="submit">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(390, 'SQL\nSELECT `paget_id` FROM `oba$chunck` WHERE `ID`=0[*GET.id*]\n\nBLOCK\n<p id="" class="section box"><a href="../?p=cic_admin_p_mod&id=[*SQL.page_id*]"> < Go back</a> | <strong>  CANCEL</strong></p>'),
(392, 'SQL\rSELECT cn.`name` AS n FROM `oba$chunck_name` AS cn, `oba$chunck` AS c WHERE c.`chunck_name_id`=cn.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\rRename container - [*SQL.n*]'),
(400, 'SQL\nSELECT `id`, `name`, `id` AS id1, `id` AS id2 FROM `oba$folder`  ORDER BY `name`\n\nBLOCK\n<tr> <td id="[*SQL.id*]"><img width="14" height="14" src="img/folder.png"/> [*SQL.name*]</td> <td width="85px"><a href="../?p=cic_admin_f_ren&id=[*SQL.id1*]"><img width="14" height="14" src="img/edit.png"/> Rename</a></td> <td width="85px"><a href="../?p=cic_admin_f_del&id=[*SQL.id2*]"><img width="14" height="14" src="img/remove.png"/> Delete</a></td></tr>'),
(402, 'SQL\rSELECT DATABASE() AS db\r\rBLOCK\rStructure - [*SQL.db*]'),
(417, '<div class="section box">\r  <h2>Edit page</h2>\r  <p><strong>Page properties</strong> (add new)</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="set_page_prop"  method="POST" action="siteman.php?action=set_page_prop&id=[*GET.id*]">\r         <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]"> \r           [*folder_template*]\r         <fieldset style="border-radius: 4px;border: 1px solid rgb(220, 220, 220); padding: 10px">\r           [*authorized*]\r         </fieldset> \r        <br>&nbsp;\r        <button type="submit"> Save </button>\r        <br>&nbsp;\r      </form>\r    </div>\r  </div>\r</div>'),
(424, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Delete page</h2>\r  <p>Are you sure?</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_page"  method="POST" action="siteman.php?action=del_page&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <strong>[*SQL.name*]</strong> - page will be removed\r        <button type="submit" style="margin: -4px 0 0 0;">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(425, 'SQL\rSELECT `name` FROM `oba$folder` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Delete folder\\group</h2>\r  <p>Are you sure?</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_folder"  method="POST" action="siteman.php?action=del_folder&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <strong>[*SQL.name*]</strong> - folder\\group will be removed\r        <button type="submit" style="margin: -4px 0 0 0;">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(426, 'SQL\rSELECT cn.`name` FROM `oba$chunck_name` AS cn, `oba$chunck` AS c WHERE c.`chunck_name_id`=cn.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Delete container</h2>\r  <p>Are you sure?</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_container"  method="POST" action="siteman.php?action=del_container&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <strong>[*SQL.name*]</strong> - container will be removed\r        <button type="submit" style="margin: -4px 0 0 0">  OK  </button> <br>\r      </form>\r    </div>\r  </div>\r</div>'),
(428, 'SQL\r\nSELECT IF(c.`chunck_type`=0, '' checked'', '''') AS txt_tp,  IF(c.`chunck_type`=1, '' checked'', '''') AS blk_tp FROM `oba$chunck` AS c, `oba$chunck_val` AS v WHERE c.`chunck_val_id`=v.`id` AND c.`id`=0[*GET.id*]\r\n\r\nBLOCK\r\n<div class="section box">\r\n  <h2>Container</h2>\r\n  <p>Edit container:</p>\r\n  <div class="box2">\r\n    <div id="txt_fld11">\r\n      <form name="ren_folder"  method="POST" action="siteman.php?action=mod_container&id=[*GET.id*]">\r\n        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r\n        <input name="chunck_type" type="radio" value="0" [*PSQL.txt_tp*] />Text\r\n        <input name="chunck_type" type="radio" value="1" [*PSQL.blk_tp*] />Block<br><br>\r\n        <textarea id="container" class="code" mode="text/html" ch_id=[*GET.id*] style="width: 100%;" name="value"></textarea><br>\r\n        <button type="submit">  Save  </button><br>&nbsp;\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>'),
(442, '<div class="section box">\r\n  <h2>Templates</h2>\r\n  <!--\r\n  <p><strong>Temlate</strong> (add new)</p>\r\n  <div class="box2">\r\n    <div id="txt_fld11">\r\n      <form name="new_templ"  method="POST" action="siteman.php?action=new_templ">\r\n        <input name="name" type="file">\r\n        <button type="submit"> Upload </button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n  -->\r\n[*folder_list_box*]\r\n</div>'),
(443, 'SQL\r\nSELECT `id` FROM `oba$template` LIMIT 1\r\n\r\nBLOCK\r\n<p><strong>Templates</strong> (list):</p>\r\n<div class="box2">\r\n  <table class="edit_list" width="100%">\r\n    <tbody>\r\n      [*group_list*]\r\n    </tbody>\r\n  </table>\r\n  <form name="update_templ"  method="POST" action="siteman.php?action=update_templ">\r\n    <button type="submit" style="margin: 15px 0 0 0;"> Update list </button>\r\n    <br><br><br>\r\n  </form>\r\n</div>\r\n'),
(446, '<table  width="100%">\r\n  <tbody>\r\n    <tr>\r\n      <td width="100px">\r\n        <img width="14" height="14" src="img/folder.png"/>  Folder:\r\n      </td>\r\n      <td width="300px">\r\n        <select name="folder_list" style="width: 100%;">\r\n          [*folder_list_box*]\r\n        </select>\r\n      </td>\r\n      <td>&nbsp;</td>\r\n      <td>\r\n        <a href="../?p=cic_admin_struct" target="_blank">\r\n          <img width="14" height="14" src="img/modify.png"/> \r\n          Edit folders >> \r\n        </a>\r\n      </td>\r\n    </tr>\r\n    <tr></tr>\r\n    <tr>\r\n      <td width="100px">\r\n        <img width="14" height="14" src="img/page.png"/>  Template:\r\n      </td>\r\n      <td width="300px">\r\n        <select name="templ_list"  style="width: 100%;">\r\n          [*group_list0*]\r\n          [*group_list*]\r\n        </select>\r\n      </td>\r\n      <td>&nbsp;</td>\r\n      <td>\r\n        <a href="../?p=cic_admin_template" target="_blank">\r\n          <img width="14" height="14" src="img/modify.png"/> Edit templates >> \r\n        </a>\r\n      </td>\r\n    </tr>\r\n  </tbody>\r\n</table>\r\n<br>\r\n'),
(450, 'SQL\r\nSELECT t.`id`, t.`name` AS n1, t.`name` AS n2  FROM `oba$template` AS t, `oba$paget` AS p WHERE (p.`template_name`!=t.name) GROUP BY t.`id`\r\n\r\nBLOCK\r\n<option id="[*SQL.id*]" value="[*SQL.n1*]">[*SQL.n2*]</option>'),
(451, 'SQL\r\nSELECT `template_name` AS t1, `template_name` AS t2 FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\n\r\nBLOCK\r\n<option selected id="0" value="[*SQL.t1*]">[*SQL.t2*]</option>'),
(453, '<!-- jQuery and jQuery UI (REQUIRED) --> \r\n<link rel="stylesheet" type="text/css" media="screen" href="filemanager/css/jquery-ui.css">\r\n<script type="text/javascript" src="filemanager/js/jquery.min.js"></script>\r\n<script type="text/javascript" src="filemanager/js/jquery-ui.min.js"></script>\r\n<!-- elFinder CSS (REQUIRED) -->		\r\n<link rel="stylesheet" type="text/css" media="screen" href="filemanager/css/elfinder.min.css">\r\n<link rel="stylesheet" type="text/css" media="screen" href="filemanager/css/theme.css">\r\n\r\n<script type="text/javascript" src="js/colorer.js"></script>\r\n\r\n<!-- elFinder JS (REQUIRED) -->\r\n<script type="text/javascript" src="filemanager/js/elfinder.min.js"></script>\r\n<!-- elFinder translation (OPTIONAL) -->\r\n<script type="text/javascript" src="filemanager/js/i18n/elfinder.ru.js"></script>\r\n<!-- elFinder initialization (REQUIRED) -->\r\n<script type="text/javascript" charset="utf-8">\r\n  $().ready(function() {\r\n    var elf = $(''#elfinder'').elfinder({\r\n      // lang: ''ru'',             // language (OPTIONAL)        \r\n      url : ''filemanager/php/connector.php''  // connector URL (REQUIRED)          \r\n    }).elfinder(''instance'');      \r\n  });    \r\n</script>'),
(458, 'SQL\r\nSELECT c.`id`, c.`id`AS id1, n.`name` AS n1, IF(c.`chunck_type`=0, ''text'', ''block'') AS typ, c.`id`AS id2, c.`id`AS id3 FROM `oba$chunck` AS c, `oba$chunck_name` AS n  WHERE c.`CHUNCK_NAME_ID`=n.`id` AND c.`PAGET_ID`=0[*GET.id*] ORDER BY n.`name`\r\n\r\nBLOCK\r\n<tr>\r\n  <td id="[*SQL.id*]">\r\n    <a title="Edit container" href="../?p=cic_admin_c_mod&id=[*SQL.id1*]">\r\n      <img width="14" height="14" src="img/page1.png"/> [*SQL.n1*]\r\n    </a>\r\n  </td>\r\n  <td width="85px">[*SQL.typ*]</td>\r\n  <td width="85px">\r\n    <a href="../?p=cic_admin_c_ren&id=[*SQL.id2*]">\r\n      <img width="14" height="14" src="img/edit.png"/> Rename\r\n    </a>\r\n  </td>\r\n  <td width="85px">\r\n    <a href="../?p=cic_admin_c_del&id=[*SQL.id3*]">\r\n      <img width="14" height="14" src="img/remove.png"/> Delete\r\n    </a>\r\n  </td>\r\n</tr>'),
(464, 'SQL\r\nSELECT `name` FROM `oba$template` WHERE `id`=0[*GET.id*]\r\n\r\nBLOCK\r\nEdit template file - [*SQL.name*]'),
(470, '<p id="" class="section box"><a href="../?p=cic_admin_template"> < Go back</a> | <strong>  TEMPLATES</strong></p>'),
(472, 'SQL\r\nDELETE FROM `oba$template` WHERE `ID`=0[*GET.id*]\r\n\r\nBLOCK\r\n'),
(473, 'Set-Cookie: admin_name=; path=/\r\nLocation: /?p=cic_admin_login\n\r'),
(474, 'Location: /?p=cic_admin_template\r\n\r\n\r\n[*command*]\r\n'),
(475, 'SQL\r\nSELECT `id`, `name` AS n1, `name` AS n2, `id` AS id2, `id` AS id3 FROM `oba$template` WHERE `id` != 1 ORDER BY `id`\r\n\r\nBLOCK\r\n<tr>\r\n  <td id="[*SQL.id*]">\r\n    <a href="../[*SQL.n1*]" title="preview">\r\n      <img width="14" height="14" src="img/page.png"/> [*SQL.n2*]\r\n    </a>\r\n  </td>\r\n  <td width="85px">\r\n    <a href="../?p=cic_admin_t_edit&id=[*SQL.id2*]" title="edit">\r\n      <img width="14" height="14" src="img/edit.png"/> Edit\r\n    </a>\r\n  </td>\r\n  <td width="85px">\r\n    <a href="../?p=cic_admin_t_del&id=[*SQL.id3*]" title="delete from list">\r\n      <img width="14" height="14" src="img/remove.png"/> Delete\r\n    </a>\r\n  </td>\r\n</tr>'),
(487, 'SQL\r\nSELECT c.`id` AS cid, n.`name` AS nn FROM `oba$chunck` AS c, `oba$chunck_name` AS n, (SELECT `paget_id` AS `pid` FROM `oba$chunck` WHERE `id`=[*GET.id*]) AS p WHERE c.`CHUNCK_NAME_ID`=n.`id` AND c.`PAGET_ID`=p.`pid` ORDER BY n.`name`\r\n\r\nBLOCK\r\n<li><a href="../?p=cic_admin_c_mod&id=[*SQL.cid*]">[*SQL.nn*]</a></li>'),
(496, 'SQL\r\nSELECT USER() AS cu, DATABASE( ) AS dbs, p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "> "), "") AS fn, p.`name` AS pname, n.`name` AS nname FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f, `oba$chunck_name` AS n, `oba$chunck` AS c WHERE p.`parent_id`=f.`id` AND c.`paget_id`=p.`id` AND c.`chunck_name_id`=n.`id` AND c.`id`=0[*GET.id*]\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href=''../?p=cic_admin_p_mod&id=[*SQL.pid*]''>\r\n          <img width="14" height="14" src="img/page.png"/>\r\n          [*SQL.fn*][*SQL.pname*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n        <ul class="sub-menu">\r\n          [*fn_pg_list*]\r\n        </ul>\r\n      </li>\r\n      <li>\r\n        <img width="14" height="14" src="img/page1.png"/>\r\n        [*SQL.nname*]\r\n        <ul class="sub-menu">\r\n          [*ch_list*]\r\n        </ul>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(498, 'SQL\r\nSELECT USER() AS cu, DATABASE( ) AS dbs, p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "&gt; "), "") AS fn, p.`name` AS pname, n.`name` AS nname FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f, `oba$chunck_name` AS n, `oba$chunck` AS c WHERE p.`parent_id`=f.`id` AND c.`paget_id`=p.`id` AND c.`chunck_name_id`=n.`id` AND c.`id`=0[*GET.id*]\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href=''../?p=cic_admin_p_mod&id=[*SQL.pid*]''>\r\n          <img width="14" height="14" src="img/page.png"/>\r\n          [*SQL.fn*][*SQL.pname*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n        <ul class="sub-menu">\r\n          [*fn_pg_list*]\r\n        </ul>\r\n      </li>\r\n      <li>\r\n        <img width="14" height="14" src="img/page1.png"/>\r\n        [*SQL.nname*]\r\n        <ul class="sub-menu">\r\n          [*ch_list*]\r\n        </ul>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(500, 'SQL\r\nSELECT  p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "&gt; "), "") AS fn, p.`name` AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.`type` >= (0-0[*GET.system*]) ORDER BY fn, pn\r\n\r\nBLOCK\r\n<li>\r\n  <a href="../?p=cic_admin_p_mod&id=[*SQL.pid*]">[*SQL.fn*][*SQL.pn*]</a>\r\n</li>'),
(501, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        File Manager\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(502, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs, `name` FROM `oba$folder` WHERE id=0[*GET.id*]\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]	\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <img width="14" height="14" src="img/folder.png"/>\r\n        [*SQL.name*]\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(505, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <img width="14" height="14" src="img/cic.png"/>\r\n        [*SQL.dbs*]\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(506, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs, IF(f.name!="", CONCAT(f.name, "&gt; "), "") AS fn, p.name AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.id=0[*GET.id*]\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <img width="14" height="14" src="img/page.png"/>\r\n        [*SQL.fn*][*SQL.pn*]\r\n        <ul class="sub-menu">\r\n          [*fn_pg_list*]\r\n        </ul>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(507, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        Change login/password\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(508, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        MySQL statement\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(509, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        Purge\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(510, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        Templates\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(511, 'SQL\r\nSELECT `name`  FROM `oba$template` WHERE `id`=[*GET.id*]\r\n\r\nBLOCK\r\n<div class="section box">\r\n  <h2>Edit template file</h2>\r\n  <p>[*SQL.name*]</p>\r\n  <div class="box2">\r\n    <div id="txt_fld11">\r\n      Use <a href="../?p=cic_admin_filemanager"><strong>File Manager</strong></a> for edit the template files\r\n    </div>\r\n  </div>\r\n</div>'),
(512, 'SQL\r\nSELECT p.`id`,  p.`id` AS id1, p.`name` AS n1, f.`name` AS fn, p.`name` AS pn, p.`id` AS id2, p.`id` AS id3 FROM `OBA$PAGET` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, ''root'') AS f  WHERE p.`parent_id`=f.`id` AND p.`type` >= (0-0[*GET.system*])  GROUP BY p.`id` ORDER BY fn, p.`name`\r\n\r\nBLOCK\r\n<tr>\r\n  <td id="[*SQL.id*]">\r\n    <a title="Edit page" href="../?p=cic_admin_p_mod&id=[*SQL.id1*]">\r\n      <img width="14" height="14" src="img/page.png"/> [*SQL.n1*]\r\n    </a>\r\n  </td>\r\n  <td width="130px">\r\n    <img width="14" height="14" src="img/folder.png"/> [*SQL.fn*]\r\n  </td>\r\n  <td width="90px">\r\n    <a href="../?p=[*SQL.pn*]">\r\n      <img width="14" height="14" src="img/page1.png"/> Preview\r\n    </a>\r\n  </td>\r\n  <td width="85px">\r\n    <a href="../?p=cic_admin_p_ren&id=[*SQL.id2*]">\r\n      <img width="14" height="14" src="img/edit.png"/> Rename\r\n    </a>\r\n  </td>\r\n  <td width="85px">\r\n    <a href="../?p=cic_admin_p_del&id=[*SQL.id3*]">\r\n      <img width="14" height="14" src="img/remove.png"/> Delete\r\n    </a>\r\n  </td>\r\n</tr>'),
(513, 'SQL\r\nSELECT  p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.`name` AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.`type` >= (0-0[*GET.system*]) ORDER BY fn, pn\r\n\r\nBLOCK\r\n<option value="[*SQL.pid*]">[*SQL.fn*][*SQL.pn*]</option>'),
(514, 'SQL\r\nSELECT  p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.`name` AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.`type` >= (0-0[*GET.system*]) ORDER BY fn, pn\r\n\r\nBLOCK\r\n<li>\r\n  <a href="../?p=cic_admin_p_mod&id=[*SQL.pid*]">[*SQL.fn*][*SQL.pn*]</a>\r\n</li>'),
(515, 'SQL\r\nSELECT IF((p.`id`=f.`id`), ''selected'', '''') AS ss, f.`id`, f.`id` AS id1, f.`name` AS n FROM (SELECT `parent_id` AS `id` FROM `oba$paget` WHERE `id`=0[*GET.id*]) AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, ''root'') AS f  \r\nORDER BY n\r\n\r\nBLOCK\r\n<option [*SQL.ss*] id="[*SQL.id*]" value="[*SQL.id1*]">[*SQL.n*]</option>'),
(516, 'SQL\r\nSELECT USER() AS cu, DATABASE() AS dbs, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.name AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.id=0[*GET.id*]\r\n\r\nBLOCK\r\n<div id="banner">\r\n  <div id="left_header">\r\n    <p>\r\n      <a href="../?p=cic_admin_logout">&lt; Exit</a>\r\n    </p>\r\n  </div>\r\n  <div  class="popup_menu">\r\n    <ul id="nav">\r\n      <li>\r\n        <a href="#">\r\n          <img width="14" height="14" src="img/host.png"/>\r\n          [*SQL.cu*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <a href="../?p=cic_admin_struct">\r\n          <img width="14" height="14" src="img/cic.png"/>\r\n          [*SQL.dbs*]\r\n        </a>&nbsp;&nbsp;&gt; \r\n      </li>\r\n      <li>\r\n        <img width="14" height="14" src="img/page.png"/>\r\n        [*SQL.fn*][*SQL.pn*]\r\n        <ul class="sub-menu">\r\n          [*fn_pg_list*]\r\n        </ul>\r\n      </li>\r\n    </ul>\r\n  </div>\r\n</div>'),
(517, 'SQL\r\nSELECT IF (`authorized`=1, ''checked'', '''') AS checkd, `condition`, `guesturl` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\n\r\nBLOCK\r\n <legend>&nbsp;<input name="authorized" type="checkbox" [*SQL.checkd*]> Make authorized&nbsp;</legend>\r\n<div id="authorized">\r\nCondition:\r\n<textarea id="condition" class="code" mode="text/x-mysql" name="condition" style="width: 100%;">[*PSQL.condition*]</textarea><br><br>\r\n<label>If rows=0 then GuestUrl:</label><input name="guesturl" type="text"value="[*PSQL.guesturl*]" style="width: 100%;"  /> \r\n</div>'),
(518, '<div class="section box">\r\n  <h2>Containers in page</h2>\r\n  <p><strong>Сreate a new container</strong></p>\r\n  <div class="box2">\r\n    <div id="txt_fld11">\r\n      <form name="new_container" method="POST" action="siteman.php?action=new_container&id_page=[*GET.id*]">\r\n        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]"> \r\n        <input name="name" type="text">\r\n        <button type="submit">Create</button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n[*page_list_box*]\r\n</div>'),
(520, 'SQL\r\nSELECT COUNT(`id`) AS cnt FROM `oba$chunck_name` WHERE `id` NOT IN (SELECT DISTINCT `chunck_name_id` FROM `oba$chunck`)\r\n\r\nBLOCK\r\n  <div class="box2">\r\n    <div id="txt_fld11">\r\n      <form name="del_folder"  method="POST" action="siteman.php?action=purge_names">\r\n        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]">\r\n        <strong>[*SQL.cnt*]</strong> - free container names found (will be deleted)\r\n        <button type="submit" style="margin: -4px 0;">  Purge  </button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n'),
(521, 'SQL\r\nSELECT COUNT(`id`) AS cnt FROM `oba$chunck_val` WHERE `id` NOT IN (SELECT DISTINCT `chunck_val_id` FROM `oba$chunck`)\r\n\r\nBLOCK\r\n  <div class="box2">\r\n    <div id="txt_fld11">\r\n      <form name="del_folder"  method="POST" action="siteman.php?action=purge_values">\r\n        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]">\r\n        <strong>[*SQL.cnt*]</strong> - free container values found (will be deleted)\r\n        <button type="submit" style="margin: -4px 0;">  Purge  </button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n'),
(522, 'SQL\r\nSELECT * FROM (SELECT COUNT(id) AS cnid FROM `oba$chunck_name` WHERE `id` NOT IN (SELECT DISTINCT `chunck_name_id` FROM `oba$chunck`)) AS t1 JOIN (SELECT COUNT(id) AS cvid FROM `oba$chunck_val` WHERE `id` NOT IN (SELECT DISTINCT `chunck_val_id` FROM `oba$chunck`)) AS t2\r\n\r\nBLOCK\r\n<div class="section box">\r\n  <p>Purge all</p>\r\n  <div class="box2">\r\n    <div id="txt_fld11">\r\n      <form name="del_folder"  method="POST" action="siteman.php?action=purge_all">\r\n        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r\n        <strong>[*SQL.cnid*]</strong> - container names and  <strong>[*SQL.cvid*]</strong> - container values will be deleted\r\n        <button type="submit" style="margin: -4px 0;">  Purge All  </button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n</div>'),
(523, '<div class="section box">\r\n  <h2>MySQL</h2>\r\n  <p>Statement:</p>\r\n  <div class="box2">\r\n    <div id="txt_fld22">\r\n      <form name="change_login"  method="POST" action="[*ENV.REQUEST_URI*]">\r\n        <textarea id="statement" class="code" mode="text/x-mysql" name="statement" style="width: 100%;">[*POST.statement*]</textarea><br>&nbsp;\r\n        <button type="submit" style="margin: -4px 0;">  OK  </button>\r\n      </form>\r\n    </div>\r\n  </div>\r\n  <p>Result:</p>\r\n  <div class="box2">\r\n    <div id="sql_result">\r\n    </div>\r\n  </div>\r\n</div>');

-- --------------------------------------------------------

--
-- Структура таблицы `oba$config`
--

CREATE TABLE IF NOT EXISTS `oba$config` (
  `key` varchar(64) NOT NULL DEFAULT '' COMMENT 'ключ',
  `value` varchar(255) NOT NULL DEFAULT '' COMMENT 'значение',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT 'описание',
  `opt1` int(11) DEFAULT NULL COMMENT 'опция 1',
  `opt2` varchar(255) DEFAULT NULL COMMENT 'опция 2',
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Конфигурация';

--
-- Дамп данных таблицы `oba$config`
--

INSERT INTO `oba$config` (`key`, `value`, `description`, `opt1`, `opt2`) VALUES
('admin', '*4ACFE3202A5FF5CF467898FC58AAB1D615029441', 'Логин и пароль администратора(по умолчанию пароль:admin)', 101, ''),
('phpmyadmin_url', 'http://localhost/tools/phpmyadmin', 'Путь к phpMyAdmin', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `oba$folder`
--

CREATE TABLE IF NOT EXISTS `oba$folder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(256) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Структура таблицы `oba$paget`
--

CREATE TABLE IF NOT EXISTS `oba$paget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(256) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `TEMPLATE_NAME` varchar(256) DEFAULT NULL,
  `AUTHORIZED` tinyint(1) DEFAULT '0',
  `CONDITION` text NOT NULL,
  `GUESTURL` varchar(256) DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=27 ;

--
-- Дамп данных таблицы `oba$paget`
--

INSERT INTO `oba$paget` (`ID`, `NAME`, `PARENT_ID`, `TEMPLATE_NAME`, `AUTHORIZED`, `CONDITION`, `GUESTURL`, `type`) VALUES
(1, 'cic_admin_struct', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(2, 'cic_admin_p_ren', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(3, 'cic_admin_p_del', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(4, 'cic_admin_f_ren', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(5, 'cic_admin_f_del', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(7, 'cic_admin_p_mod', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(8, 'cic_admin_template', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(9, 'cic_admin_c_mod', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(10, 'cic_admin_c_ren', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(11, 'cic_admin_c_del', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(12, 'cic_admin_home', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(14, 'cic_admin_tool_purge', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(15, 'cic_admin_filemanager', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(16, 'cic_admin_tool_change', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(17, 'cic_admin_login', 0, 'admin/template.htm', 0, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(18, 'cic_admin_logout', 0, 'admin/template.htm', 0, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(19, 'cic_admin_tool_mysql', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(21, 'cic_admin_t_edit', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(22, 'cic_admin_t_del', 0, 'None', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1);

-- --------------------------------------------------------

--
-- Структура таблицы `oba$template`
--

CREATE TABLE IF NOT EXISTS `oba$template` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `oba$template`
--

INSERT INTO `oba$template` (`ID`, `NAME`) VALUES
(1, 'None');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
