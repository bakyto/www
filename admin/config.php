<?php
    #################    НАСТРОЙКИ   ####################
	$dblocation = 'localhost'; // расположение БД
	$dbname = 'dbname';         // имя БД
	$dbuser = 'root';          // имя пользователя БД
	$dbpasswd = '';            // пароль к БД
    #####################################################
    
	// Устанавливаем соединение с базой данных
	$dbcn = @mysql_connect($dblocation, $dbuser, $dbpasswd);
	if (!$dbcn) exit("<p><strong>ERROR</strong></p><p>INCORRECT SERVER CONFIGS</p>" . mysql_error());
	// Выбираем базу данных
	if (! @mysql_select_db($dbname, $dbcn)) exit("<p><strong>ERROR</strong></p><p>INCORRECT DATABASE NAME</p>" . mysql_error() 
		. "<br>____________________________________________<br><p>Check the file CONFIG.PHP</p>");
	
	// Определяем версию сервера
	$query = "SELECT VERSION()";
	$ver = mysql_query($query);
	if ($ver)
    {
		$version = mysql_result($ver, 0);
		list($major, $minor) = explode(".", $version);
		// если версия выше 4.1, сообщаем серверу что будем работать с кодировкой utf8
		$ver = $major . "." . $minor;
		if((float)$ver >= 4.1)
        {
			mysql_query("SET NAMES 'utf8'");
		}
	}
?>