<?php
	//$DEFAULT_CHUNCK_NAME = 'HTTP_CONTENT';
	//$GUEST_PAGE_CONTENT = 'GUEST_PAGE_CONTENT';
	//$DEFAULT_TEMPLATE_NAME = 'None';
	define("DEFAULT_CHUNCK_NAME", "HTTP_CONTENT");
	define("GUEST_PAGE_CONTENT", "GUEST_PAGE_CONTENT");
	define("DEFAULT_TEMPLATE_NAME", "None");
	
	// Новый чанк
	function New_chunck($page_id, $ch_name) {
		$query = 'SELECT `id` FROM `oba$chunck_name` WHERE (`name`=\''. mysql_real_escape_string($ch_name) . "')"; 
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		if (mysql_num_rows($res) > 0){
			$sql = mysql_fetch_array($res);
			$ch_name_id = $sql['id'];
		}else{
			$query = 'INSERT INTO `oba$chunck_name` (`NAME`) VALUES (\'' . mysql_real_escape_string($ch_name) . "')";
			$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
			$ch_name_id = mysql_insert_id();
		}
		$query = 'SELECT `id` FROM `oba$chunck` WHERE (`chunck_name_id`=' . $ch_name_id . " AND `paget_id`=" . $page_id . ")";
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		if (mysql_num_rows($res) > 0){return -1;}
		$query = 'INSERT INTO `oba$chunck` (`chunck_name_id`, `chunck_type`, `chunck_val_id`, `paget_id`) VALUES (' . $ch_name_id . ', 0, 1, ' . $page_id.')';
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		return mysql_insert_id();
	};
	
	// Новая страница
	function New_page($p_name, $folder_id, $analog_id){
		$query = 'SELECT `id` FROM `oba$paget` WHERE (`parent_id`=' . $folder_id . " AND `name`='" . mysql_real_escape_string($p_name) . "')";
		$res = mysql_query($query);	if (!$res) exit ("ERROR: ".mysql_error());
		if (mysql_num_rows($res) > 0){ return - 1;} //'ERROR: Страница с таким названием существует в данной папке'
		$query = 'INSERT INTO `oba$paget` (`name`, `parent_id`, `template_name`) VALUES (\'' . mysql_real_escape_string($p_name) . "', " . $folder_id . ", '" . DEFAULT_TEMPLATE_NAME . "')";
        $res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		$page_id = mysql_insert_id();
		if ($analog_id == 0){
			$res = New_chunck($page_id, DEFAULT_CHUNCK_NAME);
			$res = New_chunck($page_id, GUEST_PAGE_CONTENT);
		}else{
			$query = 'SELECT `template_name`, `authorized`, `condition`, `guesturl` FROM `oba$paget` WHERE `id`=' . $analog_id;
			$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
			if (mysql_num_rows($res) > 0){
				$sql = mysql_fetch_array($res);
				$template_name = $sql['template_name'];
				$authorized = $sql['authorized'];
				$condition = $sql['condition'];
				$guesturl = $sql['guesturl'];
				$query = 'UPDATE `oba$paget` SET `authorized`=' . $authorized . 
                            ', `condition`=\'' . mysql_real_escape_string($condition) . 
                            "', `guesturl`='" . mysql_real_escape_string($guesturl) . 
                            "', `template_name`='" . mysql_real_escape_string($template_name) . "' WHERE `id`=$page_id";
				$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
			}
			$res = mysql_query('CREATE TEMPORARY TABLE `foo` AS SELECT * FROM `oba$chunck` WHERE `paget_id` = '.$analog_id); if (!$res) {exit ("ERROR: ".mysql_error());}
			$res = mysql_query('UPDATE `foo` SET `id`=NULL, `paget_id` = '.$page_id); if (!$res) {exit ("ERROR: ".mysql_error());}
			$res = mysql_query('INSERT INTO `oba$chunck` SELECT * FROM `foo`'); if (!$res) {exit ("ERROR: ".mysql_error());}
			$res = mysql_query('DROP TABLE `foo`'); if (!$res) {exit ("ERROR: ".mysql_error());}
		}
		return $page_id;
	}
	
	// Новый контейнер
	function New_container($id_page, $name){
		$query='SELECT c.`id` FROM `oba$chunck` AS c, `oba$chunck_name` AS cn WHERE c.`chunck_name_id`=cn.`id` AND c.`paget_id`='.$id_page." AND cn.`name`='" . mysql_real_escape_string($name) . "'";
		$res = mysql_query($query);	if (!$res) exit ("ERROR: ".mysql_error());
		if (mysql_num_rows($res) > 0){ return - 1;} //'ERROR: Контейнер с таким названием существует в данной странице'
		$query='SELECT `id` FROM `oba$chunck_name` WHERE `name`=\'' . mysql_real_escape_string($name) . "'";
		$res = mysql_query($query);	if (!$res) exit ("ERROR: ".mysql_error());
		if (mysql_num_rows($res) > 0){
			$sql = mysql_fetch_array($res);
			$ch_name_id = $sql['id'];
		}else{
			$query = 'INSERT INTO `oba$chunck_name` (`name`) VALUES (\'' . mysql_real_escape_string($name) . "')";
			$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
			$ch_name_id = mysql_insert_id();
		}
		$query = 'INSERT INTO `oba$chunck` (`chunck_name_id`, `chunck_type`, `chunck_val_id`, `paget_id`) VALUES ('.$ch_name_id.', 0, 1, '.$id_page.')';
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		return mysql_insert_id();
	}
	
	
	require_once "config.php";
	
	// *** ДЕЙСТВИЯ ***
	$action = $_GET["action"];
	
	// ----------- Создать папку
	if ($action == 'new_folder') { 
		if (!preg_match("|^[-0-9a-z_]{1,20}$|i", $_POST['name'])){
			echo 'ERROR: Название папки должен состоять от 1 до 20 латинских букв, цифр, и знаков "-", "_"';
			exit();
		}else{
			$query = 'SELECT `id` FROM `oba$folder` WHERE `name`=' . "'" . mysql_real_escape_string($_POST['name']) . "'";
			$res = mysql_query($query);
			if (!$res) exit ("ERROR: ".mysql_error());
			if (mysql_num_rows($res) > 0){
				echo 'ERROR: Папка с таким названием существует';
				exit();
			}
			$query = 'INSERT INTO `oba$folder` (`name`, `parent_id`) VALUES (\'' . mysql_real_escape_string($_POST['name']) . "', 0)"; 
			$res = mysql_query($query);
			if (!$res) {exit ("ERROR: " . mysql_error());}
			header("Location: ".$_SERVER['HTTP_REFERER']); // перенаправление от куда пришел
			exit();
		}
	
	// ----------- Создать страницу
	} elseif ($action == 'new_page') { 
		
		if (!preg_match("|^[-0-9a-z_]{1,64}$|i", $_POST['name'])){
			echo 'ERROR: Название страницы должен состоять от 1 до 64 латинских букв, цифр, и знаков "-", "_"';
			exit();
		}else{
			$name = $_POST['name'];
			if ($_POST['folder'] != ''){$parent_id = (int)$_POST['folder'];}else{$parent_id = 0;}
			if ($_POST['analog_page'] != ''){$analog_page = (int)$_POST['analog_page'];}else{$analog_page = 0;}
			$res = New_page($name, $parent_id, $analog_page);
			if ($res > 0){
				header("Location: ".$_SERVER['HTTP_REFERER']); // перенаправление от куда пришел
			}elseif($res = -1){
				echo 'ERROR: Страница с таким названием существует в данной папке';
				exit();
			}else{
				echo 'ERROR: #01';
			}
			exit();
		}
	
	// ----------- Переименовать папку
	} elseif ($action == 'ren_folder') {
		$name = $_POST['new_name'];
		$gotourl = $_POST['gotourl'];
		
		$id = $_GET['id'];
		if (!preg_match("|^[-0-9a-z_]{1,20}$|i", $name)){
			echo 'ERROR: Название папки должен состоять от 1 до 20 латинских букв, цифр, и знаков "-", "_"';
			exit();
		}else{
			$query = 'SELECT `id` FROM `oba$folder` WHERE `name`=\'' . mysql_real_escape_string($name) . "'";
			$res = mysql_query($query);
			if (!$res) exit ("ERROR: ".mysql_error());
			if (mysql_num_rows($res) > 0){
				echo 'ERROR: Папка с таким названием существует';
				exit();
			}
			$query = 'UPDATE `oba$folder` SET `name`=\'' . mysql_real_escape_string($name) . "' WHERE `id`=".$id; 
			$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
			header("Location: ".$gotourl);
			exit();
		}
	
	// ----------- Удалить папку
	} elseif ($action == 'del_folder') {
		$id = $_GET['id'];
		$gotourl = $_POST['gotourl'];
        $query = 'SELECT `id` FROM `oba$paget` WHERE `parent_id`=' . mysql_real_escape_string($id);
		$res = mysql_query($query);
		if (!$res) exit ("ERROR: ".mysql_error());
		if (mysql_num_rows($res) > 0){
			echo 'ERROR: Папка не пуста!';
			exit();
		}
         
		$query = 'DELETE FROM `oba$folder` WHERE `id`=' . $id; 
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		//$query = 'DELETE FROM `oba$paget` WHERE `parent_id`='.$id; 
		//$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		header("Location: ".$gotourl);
		exit();
	
	// ----------- Переименовать страницу
	} elseif ($action == 'ren_page') { 
		$name = $_POST['new_name'];
		$gotourl = $_POST['gotourl'];
		$id = $_GET['id'];
		if (!preg_match("|^[-0-9a-z_]{1,20}$|i", $name)){
			echo 'ERROR: Название страницы должен состоять от 1 до 20 латинских букв, цифр, и знаков "-", "_"'; 
			exit();
		}else{
			$query = 'SELECT `id` FROM `oba$paget` WHERE `name`=\'' . mysql_real_escape_string($name) . "' AND `parent_id`=`parent_id` AND `id`=".$id; 
			$res = mysql_query($query);
			if (!$res) exit ("ERROR: ".mysql_error());
			if (mysql_num_rows($res) > 0){
				echo 'ERROR: Страница с таким названием существует';
				exit();
			}
			$query = 'UPDATE `oba$paget` SET `name`=\'' . mysql_real_escape_string($name) . "' WHERE `id`=".$id; 
			$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
			header("Location: ".$gotourl);
			exit();
		}
		
	// ----------- Удалить страницу
	} elseif ($action == 'del_page') {
		(int)$id = $_GET['id'];
		$gotourl = $_POST['gotourl'];
		$query = 'DELETE FROM `oba$paget` WHERE `id`='.$id; 
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		header("Location: ".$gotourl);
		exit();
	
	// ----------- Сохранить свойства страницы
	} elseif ($action == 'set_page_prop') {
		$id = (int)$_GET['id'];
		$gotourl = $_POST['gotourl'];
		$parent_id = (int)$_POST['folder_list'];
		$query = 'SELECT `id` FROM `oba$paget` WHERE `id`<>`id` AND `name`=`name` AND `parent_id`='.$parent_id;
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		if (mysql_num_rows($res) > 0){
			echo 'ERROR: Не удалось переместить в другую папку. Страница с таким названием в данной папке существует.';
			exit();
		}
		if($_POST['authorized'] == 'on'){$str = 1;} else {$str = 0;}
		
		//mysql_real_escape_string - экранирование
		$query = 'UPDATE `oba$paget` SET `authorized`='.$str
			.', `parent_id`='.$parent_id
			.', `template_name`=\'' . mysql_real_escape_string($_POST['templ_list']) . "'"
			.', `condition`=\'' . mysql_real_escape_string($_POST['condition']) . "'"
			.', `guesturl`=\'' . mysql_real_escape_string($_POST['guesturl']) . "'"
			//.', `description`='.$_POST['page_desc']
			.' WHERE `id`='.$id;
		//header("Location: ".$gotourl);
		//exit($query."<br>".$gotourl);
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		header("Location: ".$gotourl);
		exit();
	
	// ----------- Создать контейнер
	} elseif ($action == 'new_container') { 
		if (!preg_match("|^[0-9a-z_]{1,64}$|i", $_POST['name'])){
			echo 'ERROR: Название контейнера должен состоять от 1 до 64 латинских букв, цифр, и знака "_"';
			exit();
		}else{
			$id_page = (int)$_GET['id_page'];
			$gotourl = $_POST['gotourl'];
			$name = $_POST['name'];
			$res = New_container($id_page, $name);
			if ($res > 0){
				header("Location: ".$_SERVER['HTTP_REFERER']); 
			}elseif($res = -1){
				echo 'ERROR: Контейнер с таким названием существует в данной странице';
				exit();
			}else{
				echo 'ERROR: #02';
			}
			header("Location: ".$gotourl);
			exit();
		}
	
	// ----------- Переименовать контейнер
	} elseif ($action == 'ren_container') { 
		$gotourl = $_POST['gotourl'];
		$name = $_POST['new_name'];
		$id = $_GET['id'];
		if (!preg_match("|^[0-9a-z_]{1,64}$|i", $name)){
			echo 'ERROR: Название контейнера должен состоять от 1 до 64 латинских букв, цифр, и знака "_"';
			exit();
		}else{
			$query='SELECT c.`id` FROM `oba$chunck` AS c, `oba$chunck_name` AS cn WHERE c.`chunck_name_id`=cn.`id` AND c.`paget_id`=' . $id . " AND cn.`name`='" . mysql_real_escape_string($name) . "'";
			$res = mysql_query($query);	if (!$res) exit ("ERROR: ".mysql_error());
			if (mysql_num_rows($res) > 0){exit('ERROR: Контейнер с таким названием существует в данной странице');}
			$query='SELECT `id` FROM `oba$chunck_name` WHERE `name`=\'' . mysql_real_escape_string($name) . "'";
			$res = mysql_query($query);	if (!$res) exit ("ERROR: ".mysql_error());
			if (mysql_num_rows($res) > 0){
				$sql = mysql_fetch_array($res);
				$ch_name_id = $sql['id'];
			}else{
				$query = 'INSERT INTO `oba$chunck_name` (`name`) VALUES (\'' . mysql_real_escape_string($name) . "')";
				$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
				$ch_name_id = mysql_insert_id();
			}
			
			
			$query = 'UPDATE `oba$chunck` SET `chunck_name_id`=' . $ch_name_id . ' WHERE `id`=' . $id; 
			$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
			header("Location: ".$gotourl);
			exit();
		}
		
	// ----------- Удалить контейнер
	} elseif ($action == 'del_container') {
		(int)$id = $_GET['id'];
		$gotourl = $_POST['gotourl'];
		$query = 'DELETE FROM `oba$chunck` WHERE `id`='.$id; 
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		header("Location: ".$gotourl);
		exit();
	
	// ----------- Получить содержание контейнера
	} elseif ($action == 'get_container_value') {
		(int)$id = $_GET['id'];
		$query='SELECT v.`val` AS val FROM `oba$chunck` AS c, `oba$chunck_val` AS v WHERE c.`chunck_val_id`=v.`id` AND c.`id`=' . $id;
		$res = mysql_query($query);	if (!$res) exit ("ERROR: ".mysql_error());
		if (mysql_num_rows($res) > 0){
			$sql = mysql_fetch_array($res);
			$val = $sql['val'];
		}else{$val="";}
		exit($val);	
	
	// ----------- Сохранить свойства контейнера
	} elseif ($action == 'mod_container') {
		(int)$id = $_GET['id'];
		$gotourl = $_POST['gotourl'];
		$ch_type = $_POST['chunck_type'];
		$ch_value = mysql_real_escape_string($_POST['value']);
		$query = 'SELECT `id` FROM `oba$chunck_val` WHERE `val`=\'' . $ch_value . "'";
		$res = mysql_query($query); if (!$res) {exit ("ERROR: #1 " . mysql_error());}
		if (mysql_num_rows($res) > 0){
			$sql = mysql_fetch_array($res);
			$ch_val_id = $sql['id'];
		}else{
			$query = 'INSERT INTO `oba$chunck_val` (`val`) VALUES (\'' . $ch_value . "')";
			$res = mysql_query($query); if (!$res) {exit ("ERROR: #2 " . mysql_error());}
			$ch_val_id = mysql_insert_id();
		}
		$query = 'UPDATE `oba$chunck` SET `chunck_val_id`=' . $ch_val_id . ', `chunck_type`=0' . $ch_type . ' WHERE `id`=' . $id;
		$res = mysql_query($query); if (!$res) {exit ("ERROR: #3 " . mysql_error());}
		header("Location: ".$gotourl);
		exit();
	
	// ----------- Сохранить свойства контейнера
	} elseif ($action == 'login') {
		$gotourl = $_POST['gotourl'];
		$u_name = $_POST['name'];
		$u_password = $_POST['password'];
		$query = 'SELECT `key` FROM `oba$config` WHERE `key`=\'' . mysql_real_escape_string($u_name) . 
                "' AND `value`=PASSWORD('" . mysql_real_escape_string($u_password) . "') AND `opt1`=101";
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		if (mysql_num_rows($res) > 0){
			header('Set-Cookie: admin_name='.$u_name.'; path=/');
		}
		header('Location: '.$gotourl);
		exit();
	
	// ----------- Очистка имен контейнеров
	} elseif ($action == 'purge_names') {
		$query = 'DELETE FROM `oba$chunck_name` WHERE `id` NOT IN (SELECT DISTINCT `chunck_name_id` FROM `oba$chunck`)';
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		$gotourl = $_POST['gotourl'];
		header('Location: '.$gotourl);
		exit();
	
	// ----------- Очистка значений контейнеров
	} elseif ($action == 'purge_values') {
		$query = 'DELETE FROM `oba$chunck_val` WHERE `id` NOT IN (SELECT DISTINCT `chunck_val_id` FROM `oba$chunck`)';
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		$gotourl = $_POST['gotourl'];
		header('Location: '.$gotourl);
		exit();
	
	// ----------- Очистка имен и значений контейнеров
	} elseif ($action == 'purge_all') {
		$query = 'DELETE FROM `oba$chunck_name` WHERE `id` NOT IN (SELECT DISTINCT `chunck_name_id` FROM `oba$chunck`)';
		$res = mysql_query($query); if (!$res) {exit ("ERROR: #1 ".mysql_error());}
		$query = 'DELETE FROM `oba$chunck_val` WHERE `id` NOT IN (SELECT DISTINCT `chunck_val_id` FROM `oba$chunck`)';
		$res = mysql_query($query); if (!$res) {exit ("ERROR: #2".mysql_error());}
		$gotourl = $_POST['gotourl'];
		header('Location: '.$gotourl);
		exit();
	
    // ----------- Обновить список шаблонов
    } elseif ($action=="update_templ") {
        // очистка таблицы
        $query = 'TRUNCATE TABLE `oba$template`';
        $res = mysql_query($query); if (!$res) {exit ("ERROR: #1 ".mysql_error());}
        $query = 'INSERT INTO `oba$template`(`name`) VALUES ' . "('None')";
        $res = mysql_query($query); if (!$res) {exit ("ERROR: #2 " . mysql_error());}
        
        $f = scandir('../');
        foreach ($f as $file){
            $regu = '/^[-0-9_a-zA-Z\-]+\.+(htm|html)$/i';
            if(preg_match($regu, $file)){
                $query = 'INSERT INTO `oba$template` (`name`) VALUES (\'' . mysql_real_escape_string($file) . "')";
                
                $res = mysql_query($query); if (!$res) {exit ("ERROR: #3 " . mysql_error());}
            }
        }
        header('Location: ' . $_SERVER['HTTP_REFERER']); //открыть страницу откуда пришел
		exit();
	// ----------- test
	} elseif ($action=="fo") {
		$query = 'SHOW TABLES';
		$res = mysql_query($query); if (!$res) {exit ("ERROR: ".mysql_error());}
		if (mysql_num_rows($res) > 0){
			$sql = mysql_fetch_array($res);
			print($sql);
		}
		exit();

	// ----------- Прочие	
	} else {
		echo $action; exit();
		
		//header("Location: /admin"); 
	}
	
	exit;
?>