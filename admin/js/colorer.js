function colorer(inmode) {
	// Подсвечивание синтаксиса
	$("textarea." + inmode).each(function(){
		var editor_html = CodeMirror.fromTextArea($(this)[0], {
		   mode: $(this).attr('mode'), 
		   lineWrapping: true,
		   lineNumbers: true,
		   fixedGutter: false,
		   //firstLineNumber: 1,
		   autoCloseTags: true,
		   extraKeys: {
			 "F11": function(cm) {
				cm.setOption("fullScreen", !cm.getOption("fullScreen"));
			 },
			 "Esc": function(cm) {
				if (cm.getOption("fullScreen")) cm.setOption("fullScreen", false);
			 }
		   }
		});
	});
}