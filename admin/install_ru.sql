-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Мар 12 2014 г., 05:32
-- Версия сервера: 5.5.25
-- Версия PHP: 5.3.13

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `cicru`
--

-- --------------------------------------------------------

--
-- Структура таблицы `oba$chunck`
--

CREATE TABLE IF NOT EXISTS `oba$chunck` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `CHUNCK_NAME_ID` int(11) DEFAULT NULL,
  `CHUNCK_TYPE` smallint(6) DEFAULT NULL,
  `CHUNCK_VAL_ID` int(11) DEFAULT NULL,
  `PAGET_ID` int(11) DEFAULT NULL,
  `WBDELETE` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=166 ;

--
-- Дамп данных таблицы `oba$chunck`
--

INSERT INTO `oba$chunck` (`ID`, `CHUNCK_NAME_ID`, `CHUNCK_TYPE`, `CHUNCK_VAL_ID`, `PAGET_ID`, `WBDELETE`) VALUES
(1, 1, 0, 1, 1, 0),
(2, 2, 0, 1, 1, 0),
(3, 1, 0, 1, 2, 0),
(4, 2, 0, 1, 2, 0),
(5, 3, 0, 3, 2, 0),
(6, 4, 1, 26, 2, 0),
(7, 5, 1, 8, 2, 0),
(8, 6, 1, 16, 2, 0),
(9, 7, 1, 25, 2, 0),
(10, 8, 1, 4, 2, 0),
(11, 9, 1, 6, 2, 0),
(12, 1, 0, 1, 3, 0),
(13, 2, 0, 1, 3, 0),
(14, 3, 0, 3, 3, 0),
(15, 8, 1, 4, 3, 0),
(16, 6, 1, 20, 3, 0),
(17, 9, 1, 6, 3, 0),
(18, 7, 1, 21, 3, 0),
(19, 5, 1, 13, 3, 0),
(20, 4, 1, 22, 3, 0),
(21, 10, 0, 15, 3, 0),
(22, 1, 0, 1, 4, 0),
(23, 2, 0, 1, 4, 0),
(24, 3, 0, 3, 4, 0),
(25, 4, 1, 19, 4, 0),
(26, 5, 1, 8, 4, 0),
(27, 6, 1, 33, 4, 0),
(28, 7, 1, 23, 4, 0),
(29, 8, 1, 4, 4, 0),
(30, 9, 1, 17, 4, 0),
(31, 1, 0, 1, 5, 0),
(32, 2, 0, 1, 5, 0),
(33, 3, 0, 3, 5, 0),
(34, 6, 0, 28, 5, 0),
(36, 7, 0, 29, 5, 0),
(37, 5, 1, 30, 5, 0),
(38, 4, 0, 31, 5, 0),
(39, 10, 0, 115, 5, 0),
(40, 1, 0, 1, 6, 0),
(41, 2, 0, 1, 6, 0),
(42, 3, 0, 3, 6, 0),
(43, 6, 1, 34, 6, 0),
(44, 7, 0, 35, 6, 0),
(45, 5, 1, 36, 6, 0),
(46, 4, 0, 37, 6, 0),
(47, 1, 0, 1, 7, 0),
(48, 2, 0, 1, 7, 0),
(49, 3, 0, 3, 7, 0),
(50, 6, 1, 38, 7, 0),
(51, 7, 0, 35, 7, 0),
(52, 5, 1, 36, 7, 0),
(53, 4, 1, 39, 7, 0),
(54, 1, 0, 1, 8, 0),
(55, 2, 0, 1, 8, 0),
(56, 3, 0, 3, 8, 0),
(57, 6, 0, 40, 8, 0),
(58, 7, 0, 46, 8, 0),
(59, 5, 1, 42, 8, 0),
(60, 4, 0, 44, 8, 0),
(61, 12, 1, 45, 8, 0),
(62, 1, 0, 1, 9, 0),
(63, 2, 0, 1, 9, 0),
(64, 3, 0, 3, 9, 0),
(65, 6, 0, 47, 9, 0),
(66, 7, 0, 1, 9, 0),
(67, 5, 0, 48, 9, 0),
(68, 4, 0, 49, 9, 0),
(69, 1, 0, 50, 10, 0),
(70, 2, 0, 1, 10, 0),
(71, 1, 0, 1, 11, 0),
(72, 2, 0, 1, 11, 0),
(73, 3, 0, 3, 11, 0),
(74, 6, 1, 51, 11, 0),
(75, 7, 0, 35, 11, 0),
(76, 5, 1, 52, 11, 0),
(77, 4, 1, 53, 11, 0),
(78, 9, 1, 6, 11, 0),
(79, 1, 0, 1, 12, 0),
(80, 2, 0, 1, 12, 0),
(81, 3, 0, 3, 12, 0),
(82, 6, 1, 55, 12, 0),
(83, 7, 0, 35, 12, 0),
(84, 5, 1, 52, 12, 0),
(85, 4, 1, 56, 12, 0),
(86, 9, 1, 6, 12, 0),
(87, 1, 0, 1, 13, 0),
(88, 2, 0, 1, 13, 0),
(89, 13, 0, 1, 13, 0),
(90, 14, 1, 64, 13, 0),
(91, 3, 0, 3, 13, 0),
(92, 6, 0, 58, 13, 0),
(93, 9, 1, 17, 13, 0),
(94, 15, 0, 1, 13, 0),
(95, 16, 1, 62, 13, 0),
(96, 17, 0, 65, 13, 0),
(97, 7, 0, 72, 13, 0),
(98, 18, 1, 66, 13, 0),
(99, 19, 1, 67, 13, 0),
(100, 5, 1, 73, 13, 0),
(101, 20, 1, 70, 13, 0),
(102, 21, 1, 69, 13, 0),
(103, 4, 1, 74, 13, 0),
(104, 22, 0, 63, 13, 0),
(105, 10, 0, 71, 13, 0),
(106, 23, 0, 68, 13, 0),
(107, 1, 0, 1, 14, 0),
(108, 2, 0, 1, 14, 0),
(109, 6, 0, 58, 14, 0),
(110, 4, 1, 77, 14, 0),
(111, 3, 0, 3, 14, 0),
(112, 5, 1, 79, 14, 0),
(113, 22, 0, 80, 14, 0),
(114, 23, 0, 83, 14, 0),
(115, 16, 1, 81, 14, 0),
(116, 18, 1, 82, 14, 0),
(117, 15, 1, 84, 14, 0),
(118, 13, 1, 85, 14, 0),
(119, 21, 1, 86, 14, 0),
(120, 20, 1, 87, 14, 0),
(121, 1, 0, 1, 15, 0),
(122, 2, 0, 1, 15, 0),
(123, 3, 0, 3, 15, 0),
(124, 6, 0, 88, 15, 0),
(125, 7, 0, 72, 15, 0),
(126, 5, 1, 79, 15, 0),
(127, 4, 0, 91, 15, 0),
(128, 16, 1, 89, 15, 0),
(129, 18, 1, 90, 15, 0),
(130, 1, 0, 1, 16, 0),
(131, 2, 0, 1, 16, 0),
(132, 3, 0, 3, 16, 0),
(133, 6, 0, 92, 16, 0),
(134, 7, 0, 93, 16, 0),
(135, 5, 1, 94, 16, 0),
(136, 4, 0, 95, 16, 0),
(137, 1, 0, 1, 17, 0),
(138, 2, 0, 1, 17, 0),
(139, 3, 0, 3, 17, 0),
(140, 6, 0, 96, 17, 0),
(141, 7, 0, 93, 17, 0),
(142, 5, 1, 97, 17, 0),
(143, 4, 0, 98, 17, 0),
(144, 10, 0, 100, 17, 0),
(145, 1, 0, 1, 18, 0),
(146, 2, 0, 1, 18, 0),
(147, 3, 0, 3, 18, 0),
(148, 6, 0, 101, 18, 0),
(149, 7, 0, 93, 18, 0),
(150, 5, 1, 102, 18, 0),
(151, 4, 0, 106, 18, 0),
(152, 24, 1, 104, 18, 0),
(153, 25, 1, 105, 18, 0),
(154, 26, 1, 103, 18, 0),
(155, 1, 0, 107, 19, 0),
(156, 2, 0, 1, 19, 0),
(157, 27, 1, 108, 19, 0),
(158, 1, 0, 1, 20, 0),
(159, 2, 0, 1, 20, 0),
(160, 3, 0, 3, 20, 0),
(161, 6, 1, 109, 20, 0),
(162, 7, 0, 110, 20, 0),
(163, 5, 1, 111, 20, 0),
(164, 4, 1, 112, 20, 0),
(165, 7, 0, 114, 14, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `oba$chunck_name`
--

CREATE TABLE IF NOT EXISTS `oba$chunck_name` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=28 ;

--
-- Дамп данных таблицы `oba$chunck_name`
--

INSERT INTO `oba$chunck_name` (`ID`, `NAME`) VALUES
(1, 'HTTP_CONTENT'),
(2, 'GUEST_PAGE_CONTENT'),
(3, 'base_href '),
(4, 'title'),
(5, 'header'),
(6, 'content'),
(7, 'footer'),
(8, 'ch_list'),
(9, 'fn_pg_list'),
(10, '_js_css'),
(12, 'phpmyadmin_url'),
(13, 'analog_list'),
(14, 'authorized'),
(15, 'folder_list'),
(16, 'folder_list_box'),
(17, 'folder_template'),
(18, 'group_list'),
(19, 'group_list0'),
(20, 'page_list'),
(21, 'page_list_box'),
(22, '_folders'),
(23, '_pages'),
(24, 'purge_names'),
(25, 'purge_values'),
(26, 'purge_all'),
(27, 'command');

-- --------------------------------------------------------

--
-- Структура таблицы `oba$chunck_val`
--

CREATE TABLE IF NOT EXISTS `oba$chunck_val` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VAL` mediumtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=116 ;

--
-- Дамп данных таблицы `oba$chunck_val`
--

INSERT INTO `oba$chunck_val` (`ID`, `VAL`) VALUES
(1, ''),
(3, 'http://[*ENV.HTTP_HOST*]/admin/'),
(4, 'SQL\rSELECT c.`id` AS cid, n.`name` AS nn FROM `oba$chunck` AS c, `oba$chunck_name` AS n, (SELECT `paget_id` AS `pid` FROM `oba$chunck` WHERE `id`=[*GET.id*]) AS p WHERE c.`CHUNCK_NAME_ID`=n.`id` AND c.`PAGET_ID`=p.`pid` ORDER BY n.`name`\r\rBLOCK\r<li><a href="../?p=cic_admin_c_mod&id=[*SQL.cid*]">[*SQL.nn*]</a></li>'),
(6, 'SQL\nSELECT  p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.`name` AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.`type` >= (0-0[*GET.system*]) ORDER BY fn, pn\n\nBLOCK\n<li>\n  <a href="../?p=cic_admin_p_mod&id=[*SQL.pid*]">[*SQL.fn*][*SQL.pn*]</a>\n</li>'),
(8, 'SQL\nSELECT USER() AS cu, DATABASE( ) AS dbs, p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.`name` AS pname, n.`name` AS nname FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f, `oba$chunck_name` AS n, `oba$chunck` AS c WHERE p.`parent_id`=f.`id` AND c.`paget_id`=p.`id` AND c.`chunck_name_id`=n.`id` AND c.`id`=0[*GET.id*]\n\nBLOCK\n<div id="banner">\n  <div id="left_header">\n    <p>\n      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\n    </p>\n  </div>\n  <div  class="popup_menu">\n    <ul id="nav">\n      <li>\n        <a href="#">\n          <img width="14" height="14" src="img/host.png"/>\n          [*SQL.cu*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <a href="../?p=cic_admin_struct">\n          <img width="14" height="14" src="img/cic.png"/>\n          [*SQL.dbs*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <a href=''../?p=cic_admin_p_mod&id=[*SQL.pid*]''>\n          <img width="14" height="14" src="img/page.png"/>\n          [*SQL.fn*][*SQL.pname*]\n        </a>&nbsp;&nbsp;&gt; \n        <ul class="sub-menu">\n          [*fn_pg_list*]\n        </ul>\n      </li>\n      <li>\n        <img width="14" height="14" src="img/page1.png"/>\n        [*SQL.nname*]\n        <ul class="sub-menu">\n          [*ch_list*]\n        </ul>\n      </li>\n    </ul>\n  </div>\n</div>'),
(13, 'SQL\nSELECT USER() AS cu, DATABASE( ) AS dbs, p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "> "), "") AS fn, p.`name` AS pname, n.`name` AS nname FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f, `oba$chunck_name` AS n, `oba$chunck` AS c WHERE p.`parent_id`=f.`id` AND c.`paget_id`=p.`id` AND c.`chunck_name_id`=n.`id` AND c.`id`=0[*GET.id*]\n\nBLOCK\n<div id="banner">\n  <div id="left_header">\n    <p>\n      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\n    </p>\n  </div>\n  <div  class="popup_menu">\n    <ul id="nav">\n      <li>\n        <a href="#">\n          <img width="14" height="14" src="img/host.png"/>\n          [*SQL.cu*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <a href="../?p=cic_admin_struct">\n          <img width="14" height="14" src="img/cic.png"/>\n          [*SQL.dbs*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <a href=''../?p=cic_admin_p_mod&id=[*SQL.pid*]''>\n          <img width="14" height="14" src="img/page.png"/>\n          [*SQL.fn*][*SQL.pname*]\n        </a>&nbsp;&nbsp;&gt; \n        <ul class="sub-menu">\n          [*fn_pg_list*]\n        </ul>\n      </li>\n      <li>\n        <img width="14" height="14" src="img/page1.png"/>\n        [*SQL.nname*]\n        <ul class="sub-menu">\n          [*ch_list*]\n        </ul>\n      </li>\n    </ul>\n  </div>\n</div>'),
(15, '<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">\r<link rel="stylesheet" href="js/codemirror/addon/display/fullscreen.css">\r\r<script src="js/jquery.js"></script>\r<script src="js/codemirror/lib/codemirror.js"></script>\r<script src="js/codemirror/addon/display/fullscreen.js"></script>\r<script src="js/codemirror/addon/edit/closetag.js"></script>\r<script src="js/codemirror/mode/htmlmixed.js"></script>\r<script src="js/codemirror/mode/javascript.js"></script>\r<script src="js/codemirror/mode/xml.js"></script>\r<script src="js/codemirror/mode/php.js"></script>\r<script src="js/codemirror/mode/sql.js"></script>\r\r<script src="js/colorer.js"></script>\r<script src="js/container_textarea.js"></script>'),
(16, 'SQL\rSELECT cn.`name` FROM `oba$chunck_name` AS cn, `oba$chunck` AS c WHERE c.`chunck_name_id`=cn.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Удалить контейнер</h2>\r  <p>Хотите удалить?</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_container"  method="POST" action="siteman.php?action=del_container&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <strong>[*SQL.name*]</strong> - контейнер будет удален\r        <button type="submit" style="margin: -4px 0 0 0">  OK  </button> <br>\r      </form>\r    </div>\r  </div>\r</div>'),
(17, 'SQL\rSELECT  p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.`name` AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.`type` >= (0-0[*GET.system*]) ORDER BY fn, pn\r\rBLOCK\r<li>\r  <a href="../?p=cic_admin_p_mod&id=[*SQL.pid*]">[*SQL.fn*][*SQL.pn*]</a>\r</li>'),
(19, 'SQL\rSELECT cn.`name` FROM `oba$chunck_name` AS cn, `oba$chunck` AS c WHERE c.`chunck_name_id`=cn.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\rУдаление контейнера - [*SQL.name*]'),
(20, 'SQL\rSELECT IF(c.`chunck_type`=0, '' checked'', '''') AS txt_tp,  IF(c.`chunck_type`=1, '' checked'', '''') AS blk_tp FROM `oba$chunck` AS c, `oba$chunck_val` AS v WHERE c.`chunck_val_id`=v.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Контейнер</h2>\r  <p>Редактирование контейнера:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="ren_folder"  method="POST" action="siteman.php?action=mod_container&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="chunck_type" type="radio" value="0" [*PSQL.txt_tp*] />Text\r        <input name="chunck_type" type="radio" value="1" [*PSQL.blk_tp*] />Block<br><br>\r        <textarea id="container" class="code" mode="text/html" ch_id=[*GET.id*] style="width: 100%;" name="value"></textarea><br>\r        <button type="submit">  Сохранить  </button><br>&nbsp;\r      </form>\r    </div>\r  </div>\r</div>'),
(21, 'SQL\rSELECT `paget_id` FROM `oba$chunck` WHERE `ID`=0[*GET.id*]\r\rBLOCK\r<p id="" class="section box"><a href="../?p=cic_admin_p_mod&id=[*SQL.paget_id*]"> &lt; Назад</a> | <strong> РЕДАКТОР СТРАНИЦ </strong></p>'),
(22, 'SQL\rSELECT n.`name` AS nn, p.`name` AS pn FROM `oba$paget` AS p, `oba$chunck_name` AS n, `oba$chunck` AS c WHERE c.`paget_id`=p.`id` AND c.`chunck_name_id`=n.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\rКонтейнер - [*SQL.nn*] ([*SQL.pn*])'),
(23, 'SQL\rSELECT `paget_id` FROM `oba$chunck` WHERE `ID`=0[*GET.id*]\r\rBLOCK\r<p id="" class="section box"><a href="../?p=cic_admin_p_mod&id=[*SQL.paget_id*]"> &lt; Назад</a> | <strong>  ОТМЕНА</strong></p>'),
(25, 'SQL\rSELECT `paget_id` FROM `oba$chunck` WHERE `ID`=0[*GET.id*]\r\rBLOCK\r<p id="" class="section box"><a href="../?p=cic_admin_p_mod&id=[*SQL.page_id*]"> &lt; Назад</a> | <strong> ОТМЕНА</strong></p>'),
(26, 'SQL\rSELECT cn.`name` AS n FROM `oba$chunck_name` AS cn, `oba$chunck` AS c WHERE c.`chunck_name_id`=cn.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\rПереименование контейнера - [*SQL.n*]'),
(28, '<div class="section box">\r  <h2>Файловый менеджер</h2>\r  <p>Вы можете создавать, редактировать, удалять, переименовать свои файлы</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <!-- Element where elFinder will be created (REQUIRED) -->\r      <div id="elfinder"></div>\r    </div>\r  </div>\r</div>'),
(29, '<p id="" class="section box"><a href="../?p=cic_admin_home"> &lt; Перейти</a> | <strong>  ДОМОЙ</strong></p>'),
(30, 'SQL\nSELECT USER() AS cu, DATABASE() AS dbs\n\nBLOCK\n<div id="banner">\n  <div id="left_header">\n    <p>\n      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\n    </p>\n  </div>\n  <div  class="popup_menu">\n    <ul id="nav">\n      <li>\n        <a href="#">\n          <img width="14" height="14" src="img/host.png"/>\n          [*SQL.cu*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <a href="../?p=cic_admin_struct">\n          <img width="14" height="14" src="img/cic.png"/>\n          [*SQL.dbs*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        File Manager\n      </li>\n    </ul>\n  </div>\n</div>'),
(31, 'Файловый менеджер'),
(33, 'SQL\rSELECT cn.`name` AS n FROM `oba$chunck_name` AS cn, `oba$chunck` AS c WHERE c.`chunck_name_id`=cn.`id` AND c.`id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Переименовать контейнер</h2>\r  <p>Новое имя:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="ren_container"  method="POST" action="siteman.php?action=ren_container&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="new_name" type="text" value="[*SQL.n*]">\r        <button type="submit">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>\r'),
(34, 'SQL\rSELECT `name` FROM `oba$folder` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Удалить папку\\группу</h2>\r  <p>Хотите удалить?</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_folder"  method="POST" action="siteman.php?action=del_folder&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <strong>[*SQL.name*]</strong> - папка\\группа будет удалена\r        <button type="submit" style="margin: -4px 0 0 0;">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(35, '<p id="" class="section box"><a href="../?p=cic_admin_struct"> &lt; Назад</a> | <strong>  ОТМЕНА</strong></p>'),
(36, 'SQL\nSELECT USER() AS cu, DATABASE() AS dbs, `name` FROM `oba$folder` WHERE id=0[*GET.id*]\n\nBLOCK\n<div id="banner">\n  <div id="left_header">\n    <p>\n      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\n    </p>\n  </div>\n  <div  class="popup_menu">\n    <ul id="nav">\n      <li>\n        <a href="#">\n          <img width="14" height="14" src="img/host.png"/>\n          [*SQL.cu*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <a href="../?p=cic_admin_struct">\n          <img width="14" height="14" src="img/cic.png"/>\n          [*SQL.dbs*]	\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <img width="14" height="14" src="img/folder.png"/>\n        [*SQL.name*]\n      </li>\n    </ul>\n  </div>\n</div>'),
(37, 'SQL\rSELECT `name` FROM `oba$folder` WHERE `id`=0[*GET.id*]\r\rBLOCK\rУдалить папку/группу - [*SQL.name*]'),
(38, 'SQL\rSELECT `name` FROM `oba$folder` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Переименовать папку\\группу</h2>\r  <p>Новое имя:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="ren_folder"  method="POST" action="siteman.php?action=ren_folder&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="new_name" type="text" value="[*SQL.name*]">\r        <button type="submit">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(39, 'SQL\rSELECT `name` FROM `oba$folder` WHERE `id`=0[*GET.id*]\r\rBLOCK\rПереименовать папку/группу - [*SQL.name*]'),
(40, '<div class="section box">\n  <h2>Главная</h2>\n  <p>Основные</p>\n  <div class="box2">\n    <div id="txt_fld11">\n      <table>  \n        <tbody>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_struct"><strong>Структура</strong></a></td><td> - конструирование сайта</td>\n          </tr>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_filemanager"><strong>Файл менеджер</strong></a></td><td> - Создание, изменение, переименование, удаление файлов сайта</td>\n          </tr>\n          <tr>\n            <td width="180px"><a href="[*phpmyadmin_url*]"><strong>PhpMyAdmin</strong></a></td><td> - Администрирование баз данных с помощью phpMyAdmin</td>\n          </tr>\n        </tbody>\n      </table>  \n    </div>\n  </div>\n  <p>Инструменты</p>\n  <div class="box2">\n    <div id="txt_fld11">\n      <table>  \n        <tbody>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_tool_purge"><strong>Очистка</strong></a></td><td> - Очистить БД от не использованных имен и значений контейнеров</td>\n          </tr>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_tool_change"><strong>Изменить логин/пароль</strong></a></td><td> - Сменить логин и пароль администратора</td>\n          </tr>\n          <tr>\n            <td width="180px"><a href="../?p=cic_admin_tool_mysql"><strong>MySQL запрос</strong></a></td><td> - Выполнение MySQL запросов</td>\n          </tr>\n        </tbody>\n      </table>  \n    </div>\n  </div>\n</div>'),
(42, 'SQL\nSELECT USER() AS cu, DATABASE() AS dbs\n\nBLOCK\n<div id="banner">\n  <div id="left_header">\n    <p>\n      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\n    </p>\n  </div>\n  <div  class="popup_menu">\n    <ul id="nav">\n      <li>\n        <a href="#">\n          <img width="14" height="14" src="img/host.png"/>\n          [*SQL.cu*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <img width="14" height="14" src="img/cic.png"/>\n        [*SQL.dbs*]\n      </li>\n    </ul>\n  </div>\n</div>'),
(44, 'Administrator - [*COOKIE.admin_name*]'),
(45, 'SQL\rSELECT `value` FROM `oba$config` WHERE `key`=''phpmyadmin_url''\r\rBLOCK\r[*SQL.value*]'),
(46, '<p id="" class="section box"><a href="../?p=cic_admin_logout"> &lt; Выйти </a> | <strong> ВЫХОД</strong></p>'),
(47, '<div class="section box">\r  <h2>Вход</h2>\r  <p>Введите имя пользователя и пароль</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="login"  method="POST" action="siteman.php?action=login">\r        <input name="gotourl" type="hidden" value="../?p=cic_admin_home">\r        <input name="name" type="text" placeholder="имя пользователя" value="" >\r        <input name="password" type="password" placeholder="пароль" value="" >\r        <button type="submit">  ОК  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(48, '<div id="banner">\n    <p>ДОБРО ПОЖАЛОВАТЬ В CIC ADMIN</p>\n</div>'),
(49, 'Вход в cicAdmin'),
(50, 'Set-Cookie: admin_name=; path=/\rLocation: /?p=cic_admin_login\r\r\r'),
(51, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Удалить страницу</h2>\r  <p>Хотите удалить?</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_page"  method="POST" action="siteman.php?action=del_page&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <strong>[*SQL.name*]</strong>  - страница будет удалена\r        <button type="submit" style="margin: -4px 0 0 0;">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(52, 'SQL\nSELECT USER() AS cu, DATABASE() AS dbs, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.name AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.id=0[*GET.id*]\n\nBLOCK\n<div id="banner">\n  <div id="left_header">\n    <p>\n      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\n    </p>\n  </div>\n  <div  class="popup_menu">\n    <ul id="nav">\n      <li>\n        <a href="#">\n          <img width="14" height="14" src="img/host.png"/>\n          [*SQL.cu*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <a href="../?p=cic_admin_struct">\n          <img width="14" height="14" src="img/cic.png"/>\n          [*SQL.dbs*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <img width="14" height="14" src="img/page.png"/>\n        [*SQL.fn*][*SQL.pn*]\n        <ul class="sub-menu">\n          [*fn_pg_list*]\n        </ul>\n      </li>\n    </ul>\n  </div>\n</div>'),
(53, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\rУдалить страницу - [*SQL.name*]'),
(55, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Переименовать страницу</h2>\r  <p>Новое имя:</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="ren_page"  method="POST" action="siteman.php?action=ren_page&id=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <input name="new_name" type="text" value="[*SQL.name*]">\r        <button type="submit">  OK  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(56, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\rПереименовать страницу - [*SQL.name*]'),
(58, '[*_folders*]\r[*_pages*]'),
(62, 'SQL\rSELECT IF((p.`id`=f.`id`), ''selected'', '''') AS ss, f.`id`, f.`id` AS id1, f.`name` AS n FROM (SELECT `parent_id` AS `id` FROM `oba$paget` WHERE `id`=0[*GET.id*]) AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, ''root'') AS f  \r\rBLOCK\r<option [*SQL.ss*] id="[*SQL.id*]" value="[*SQL.id1*]">[*SQL.n*]</option>'),
(63, '<div class="section box">\r  <h2>Редактировать страницу</h2>\r  <p><strong>Свойтсва страницы</strong></p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="set_page_prop"  method="POST" action="siteman.php?action=set_page_prop&id=[*GET.id*]">\r         <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]"> \r           [*folder_template*]\r         <fieldset style="border-radius: 4px;border: 1px solid rgb(220, 220, 220); padding: 10px">\r           [*authorized*]\r         </fieldset> \r        <br>&nbsp;\r        <button type="submit"> Сохранить </button>\r        <br>&nbsp;\r      </form>\r    </div>\r  </div>\r</div>'),
(64, 'SQL\nSELECT IF (`authorized`=1, ''checked'', '''') AS checkd, `condition`, `guesturl` FROM `oba$paget` WHERE `id`=0[*GET.id*]\n\nBLOCK\n <legend>&nbsp;<input name="authorized" type="checkbox" [*SQL.checkd*]> Защищенный&nbsp;</legend>\n<div id="authorized">\nУсловие:\n<textarea id="condition" class="code" mode="text/x-mysql" name="condition" style="width: 100%;">[*PSQL.condition*]</textarea><br><br>\n<label>Если  rows=0 тогда перейти по URL(можно указать относительный путь):</label><input name="guesturl" type="text"value="[*PSQL.guesturl*]" style="width: 100%;"  /> \n</div>'),
(65, '<table  width="100%">\r  <tbody>\r    <tr>\r      <td width="100px">\r        <img width="14" height="14" src="img/folder.png"/>  Папка:\r      </td>\r      <td width="300px">\r        <select name="folder_list" style="width: 100%;">\r          [*folder_list_box*]\r        </select>\r      </td>\r      <td>&nbsp;</td>\r      <td>\r        <a href="../?p=cic_admin_struct" target="_blank">\r          <img width="14" height="14" src="img/modify.png"/> \r          Изменить папки &gt;&gt; \r        </a>\r      </td>\r    </tr>\r    <tr></tr>\r    <tr>\r      <td width="100px">\r        <img width="14" height="14" src="img/page.png"/>  Шаблон:\r      </td>\r      <td width="300px">\r        <select name="templ_list"  style="width: 100%;">\r          [*group_list0*]\r          [*group_list*]\r        </select>\r      </td>\r      <td>&nbsp;</td>\r      <td>\r        <a href="../?p=cic_admin_template" target="_blank">\r          <img width="14" height="14" src="img/modify.png"/> Изменить шаблоны &gt;&gt; \r        </a>\r      </td>\r    </tr>\r  </tbody>\r</table>\r<br>\r'),
(66, 'SQL\rSELECT t.`id`, t.`name` AS n1, t.`name` AS n2  FROM `oba$template` AS t, `oba$paget` AS p WHERE (p.`template_name`!=t.name) GROUP BY t.`id`\r\rBLOCK\r<option id="[*SQL.id*]" value="[*SQL.n1*]">[*SQL.n2*]</option>'),
(67, 'SQL\rSELECT `template_name` AS t1, `template_name` AS t2 FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\r<option selected id="0" value="[*SQL.t1*]">[*SQL.t2*]</option>'),
(68, '<div class="section box">\r  <h2>Контейнеры страницы</h2>\r  <p><strong>Создать новый контейнер</strong></p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="new_container" method="POST" action="siteman.php?action=new_container&id_page=[*GET.id*]">\r        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]"> \r        <input name="name" type="text">\r        <button type="submit">Создать</button>\r      </form>\r    </div>\r  </div>\r[*page_list_box*]\r</div>'),
(69, 'SQL\rSELECT `id` FROM `oba$chunck` WHERE `paget_id`=0[*GET.id*] LIMIT 1\r\rBLOCK\r<p><strong>Контейнеры</strong> (список):</p>\r  <div class="box2">\r    <table  width="100%" class="edit_list">\r      <tbody>\r       [*page_list*]\r      </tbody>\r    </table>\r  </div>'),
(70, 'SQL\rSELECT c.`id`, c.`id`AS id1, n.`name` AS n1, IF(c.`chunck_type`=0, ''text'', ''block'') AS typ, c.`id`AS id2, c.`id`AS id3 FROM `oba$chunck` AS c, `oba$chunck_name` AS n  WHERE c.`CHUNCK_NAME_ID`=n.`id` AND c.`PAGET_ID`=0[*GET.id*] ORDER BY n.`name`\r\rBLOCK\r<tr>\r  <td id="[*SQL.id*]">\r    <a title="Редактировать контейнер" href="../?p=cic_admin_c_mod&id=[*SQL.id1*]">\r      <img width="14" height="14" src="img/page1.png"/> [*SQL.n1*]\r    </a>\r  </td>\r  <td width="85px">[*SQL.typ*]</td>\r  <td width="85px">\r    <a title="Переименовать" href="../?p=cic_admin_c_ren&id=[*SQL.id2*]">\r      <img width="14" height="14" src="img/edit.png"/> Rename\r    </a>\r  </td>\r  <td width="85px">\r    <a title="Удалить" href="../?p=cic_admin_c_del&id=[*SQL.id3*]">\r      <img width="14" height="14" src="img/remove.png"/> Delete\r    </a>\r  </td>\r</tr>'),
(71, '<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">\r<link rel="stylesheet" href="js/codemirror/addon/display/fullscreen.css">\r\r\r<script src="js/jquery.js"></script>\r<script src="js/codemirror/lib/codemirror.js"></script>\r<script src="js/codemirror/addon/display/fullscreen.js"></script>\r<script src="js/codemirror/mode/sql.js"></script>\r\r<script src="js/colorer.js"></script>\r<script src="js/main.js"></script>'),
(72, '<p id="" class="section box"><a href="../?p=cic_admin_struct"> &lt; Перейти</a> | <strong>СТРУКТУРА</strong></p>'),
(73, 'SQL\rSELECT USER() AS cu, DATABASE() AS dbs, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.name AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.id=0[*GET.id*]\r\rBLOCK\r<div id="banner">\r  <div id="left_header">\r    <p>\r      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\r    </p>\r  </div>\r  <div  class="popup_menu">\r    <ul id="nav">\r      <li>\r        <a href="#">\r          <img width="14" height="14" src="img/host.png"/>\r          [*SQL.cu*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        <a href="../?p=cic_admin_struct">\r          <img width="14" height="14" src="img/cic.png"/>\r          [*SQL.dbs*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        <img width="14" height="14" src="img/page.png"/>\r        [*SQL.fn*][*SQL.pn*]\r        <ul class="sub-menu">\r          [*fn_pg_list*]\r        </ul>\r      </li>\r    </ul>\r  </div>\r</div>'),
(74, 'SQL\rSELECT `name` FROM `oba$paget` WHERE `id`=0[*GET.id*]\r\rBLOCK\rРедактирование страницы - [*SQL.name*]'),
(77, 'SQL\rSELECT DATABASE() AS db\r\rBLOCK\rСтруктура - [*SQL.db*]'),
(79, 'SQL\rSELECT USER() AS cu, DATABASE() AS dbs\r\rBLOCK\r<div id="banner">\r  <div id="left_header">\r    <p>\r      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\r    </p>\r  </div>\r  <div  class="popup_menu">\r    <ul id="nav">\r      <li>\r        <a href="#">\r          <img width="14" height="14" src="img/host.png"/>\r          [*SQL.cu*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        <img width="14" height="14" src="img/cic.png"/>\r        [*SQL.dbs*]\r      </li>\r    </ul>\r  </div>\r</div>'),
(80, '<div class="section box">\r  <h2>Папки\\Группы</h2>\r  <p><strong>Создать папку\\группу</strong></p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="new_folder"  method="POST" action="siteman.php?action=new_folder">\r        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]">\r        <input name="name" type="text">\r        <button type="submit">Создать</button>\r      </form>\r    </div>\r  </div>\r[*folder_list_box*]\r</div>'),
(81, 'SQL\rSELECT `id` FROM `oba$folder` WHERE `type` >= (0-0[*GET.system*])  LIMIT 1\r\rBLOCK\r<p><strong>Папки/Группы</strong> (список):</p>\r<div class="box2">\r  <table  width="100%" class="edit_list">\r    <tbody>\r      [*group_list*]\r    </tbody>\r  </table>\r</div>'),
(82, 'SQL\rSELECT `id`, `name`, `id` AS id1, `id` AS id2 FROM `oba$folder`  ORDER BY `name`\r\rBLOCK\r<tr>\r  <td id="[*SQL.id*]">\r    <img width="14" height="14" src="img/folder.png"/> [*SQL.name*]\r  </td> \r  <td width="85px">\r    <a title="Переименовать" href="../?p=cic_admin_f_ren&id=[*SQL.id1*]">\r      <img width="14" height="14" src="img/edit.png"/> Rename\r    </a>\r  </td>\r  <td width="85px">\r    <a title="Удалить" href="../?p=cic_admin_f_del&id=[*SQL.id2*]">\r      <img width="14" height="14" src="img/remove.png"/> Delete\r    </a>\r  </td>\r</tr>'),
(83, '<div class="section box">\r  <h2>Страницы</h2>\r  <p><strong>Создать страницу</strong></p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="new_page" method="POST" action="siteman.php?action=new_page">\r        <input name="name" type="text">\r        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]">\r        <select name="folder">\r          <option value="0" selected>выберите папку (root)</option>\r         [*folder_list*]\r        </select>\r        <select name="analog_page">\r          <option value="0" selected>выберите аналог (none)</option>\r         [*analog_list*]\r        </select>\r        <button type="submit">Создать</button>\r      </form>\r    </div>\r  </div>\r[*page_list_box*]\r</div>'),
(84, 'SQL\rSELECT `id`, `name`  FROM `oba$folder` WHERE `type` >= (0-0[*GET.system*]) ORDER BY `name`\r\rBLOCK\r<option value="[*SQL.id*]">[*SQL.name*]</option>'),
(85, 'SQL\rSELECT  p.`id` AS pid, IF(f.name!="", CONCAT(f.name, "&gt;"), "") AS fn, p.`name` AS pn FROM `oba$paget` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, '''') AS f WHERE p.`parent_id`=f.`id` AND p.`type` >= (0-0[*GET.system*]) ORDER BY fn, pn\r\rBLOCK\r<option value="[*SQL.pid*]">[*SQL.fn*][*SQL.pn*]</option>'),
(86, 'SQL\rSELECT `id` FROM `oba$paget` WHERE `type` >= (0-0[*GET.system*])  LIMIT 1\r\rBLOCK\r<p><strong>Страницы</strong> (список):</p>\r<div class="box2">\r  <table  width="100%" class="edit_list">\r    <tbody>\r      [*page_list*]\r    </tbody>\r  </table>\r</div>'),
(87, 'SQL\rSELECT p.`id`,  p.`id` AS id1, p.`name` AS n1, f.`name` AS fn, p.`name` AS pn, p.`id` AS id2, p.`id` AS id3 FROM `OBA$PAGET` AS p, (SELECT `id`, `name` FROM `oba$folder` UNION SELECT 0, ''root'') AS f  WHERE p.`parent_id`=f.`id` AND p.`type` >= (0-0[*GET.system*])  GROUP BY p.`id` ORDER BY fn, p.`name`\r\rBLOCK\r<tr>\r  <td id="[*SQL.id*]">\r    <a title="Редактировать страницу" href="../?p=cic_admin_p_mod&id=[*SQL.id1*]">\r      <img width="14" height="14" src="img/page.png"/> [*SQL.n1*]\r    </a>\r  </td>\r  <td width="130px">\r    <img width="14" height="14" src="img/folder.png"/> [*SQL.fn*]\r  </td>\r  <td width="90px">\r    <a title="Предварительный просмотр страницы" href="../?p=[*SQL.pn*]">\r      <img width="14" height="14" src="img/page1.png"/> Preview\r    </a>\r  </td>\r  <td width="85px">\r    <a title="Переименовать страницу" href="../?p=cic_admin_p_ren&id=[*SQL.id2*]">\r      <img width="14" height="14" src="img/edit.png"/> Rename\r    </a>\r  </td>\r  <td width="85px">\r    <a title="Удалить страницу" href="../?p=cic_admin_p_del&id=[*SQL.id3*]">\r      <img width="14" height="14" src="img/remove.png"/> Delete\r    </a>\r  </td>\r</tr>'),
(88, '<div class="section box">\r  <h2>Шаблоны</h2>\r  <!--\r  <p><strong>Шаблон</strong> (добавить)</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="new_templ"  method="POST" action="siteman.php?action=new_templ">\r        <input name="name" type="file">\r        <button type="submit"> Загрузить </button>\r      </form>\r    </div>\r  </div>\r  -->\r[*folder_list_box*]\r</div>'),
(89, 'SQL\rSELECT `id` FROM `oba$template` LIMIT 1\r\rBLOCK\r<p><strong>Шаблоны</strong> (список):</p>\r<div class="box2">\r  <table class="edit_list" width="100%">\r    <tbody>\r      [*group_list*]\r    </tbody>\r  </table>\r  <form name="update_templ"  method="POST" action="siteman.php?action=update_templ">\r    <button type="submit" style="margin: 15px 0 0 0;"> Обновить список </button>\r    <br><br><br>\r  </form>\r</div>\r'),
(90, 'SQL\rSELECT `id`, `name` AS n1, `name` AS n2, `id` AS id2, `id` AS id3 FROM `oba$template` WHERE `id` != 1 ORDER BY `id`\r\rBLOCK\r<tr>\r  <td id="[*SQL.id*]">\r    <a href="../[*SQL.n1*]" title="Просмотр">\r      <img width="14" height="14" src="img/page.png"/> [*SQL.n2*]\r    </a>\r  </td>\r  <td width="85px">\r    <a href="../?p=cic_admin_t_edit&id=[*SQL.id2*]" title="Изменить">\r      <img width="14" height="14" src="img/edit.png"/> Edit\r    </a>\r  </td>\r  <td width="85px">\r    <a href="../?p=cic_admin_t_del&id=[*SQL.id3*]" title="Удалить из списка">\r      <img width="14" height="14" src="img/remove.png"/> Delete\r    </a>\r  </td>\r</tr>'),
(91, 'Шаблоны'),
(92, '<div class="section box">\n  <h2>Изменить логин и пароль</h2>\n  <p>Сменить логин:</p>\n  <div class="box2">\n    <div id="txt_fld11">\n      <form name="change_login"  method="POST" action="siteman.php?action=change_login">\n        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\n        <input name="new_name" type="text" placeholder="Новое имя" value="" required >\n        <input name="password" type="password" placeholder="Текущий пароль" value="" required >\n        <button type="submit">  OK  </button>\n      </form>\n    </div>\n  </div>\n  <p>Сменить пароль:</p>\n  <div class="box2">\n    <div id="txt_fld11">\n      <form name="change_password"  method="POST" action="siteman.php?action=change_password">\n        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\n        <input name="new_name" type="password" placeholder="Новый пароль" value="" required >\n        <input name="new_name" type="password" placeholder="Еще раз..." value="" required >\n        <input name="password" type="password" placeholder="Текущий пароль" value="" required >\n        <button type="submit">  OK  </button>\n      </form>\n    </div>\n  </div>\n</div>'),
(93, '<p id="" class="section box"><a href="../?p=cic_admin_home"> &lt; Назад</a> | <strong>  ОТМЕНА</strong></p>'),
(94, 'SQL\nSELECT USER() AS cu, DATABASE() AS dbs\n\nBLOCK\n<div id="banner">\n  <div id="left_header">\n    <p>\n      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\n    </p>\n  </div>\n  <div  class="popup_menu">\n    <ul id="nav">\n      <li>\n        <a href="#">\n          <img width="14" height="14" src="img/host.png"/>\n          [*SQL.cu*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        <a href="../?p=cic_admin_struct">\n          <img width="14" height="14" src="img/cic.png"/>\n          [*SQL.dbs*]\n        </a>&nbsp;&nbsp;&gt; \n      </li>\n      <li>\n        Сменить логин/пароль\n      </li>\n    </ul>\n  </div>\n</div>'),
(95, 'Изменить логин и пароль - [*COOKIE.admin_name*]'),
(96, '<div class="section box">\r  <h2>MySQL</h2>\r  <p>Запрос:</p>\r  <div class="box2">\r    <div id="txt_fld22">\r      <form name="change_login"  method="POST" action="[*ENV.REQUEST_URI*]">\r        <textarea id="statement" class="code" mode="text/x-mysql" name="statement" style="width: 100%;">[*POST.statement*]</textarea><br>&nbsp;\r        <button type="submit" style="margin: -4px 0;">  OK  </button>\r      </form>\r    </div>\r  </div>\r  <p>Результат:</p>\r  <div class="box2">\r    <div id="sql_result">\r    </div>\r  </div>\r</div>'),
(97, 'SQL\rSELECT USER() AS cu, DATABASE() AS dbs\r\rBLOCK\r<div id="banner">\r  <div id="left_header">\r    <p>\r      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\r    </p>\r  </div>\r  <div  class="popup_menu">\r    <ul id="nav">\r      <li>\r        <a href="#">\r          <img width="14" height="14" src="img/host.png"/>\r          [*SQL.cu*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        <a href="../?p=cic_admin_struct">\r          <img width="14" height="14" src="img/cic.png"/>\r          [*SQL.dbs*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        MySQL запрос\r      </li>\r    </ul>\r  </div>\r</div>'),
(98, 'MySQL запрос'),
(100, '<link rel="stylesheet" href="js/codemirror/lib/codemirror.css">\r<link rel="stylesheet" href="js/codemirror/addon/display/fullscreen.css">\r\r\r<script src="js/jquery.js"></script>\r<script src="js/codemirror/lib/codemirror.js"></script>\r<script src="js/codemirror/addon/display/fullscreen.js"></script>\r<script src="js/codemirror/mode/sql.js"></script>\r\r<script src="js/colorer.js"></script>\r<script src="js/sql_result.js"></script>\r<script src="js/main.js"></script>'),
(101, '<div class="section box">\r  <h2>Очистка таблиц (имена контейнеров и значения)</h2>\r  <p>Имена контейнеров</p>[*purge_names*]\r  <p>Значения контейнеров</p>[*purge_values*]\r</div>\r[*purge_all*]'),
(102, 'SQL\rSELECT USER() AS cu, DATABASE() AS dbs\r\rBLOCK\r<div id="banner">\r  <div id="left_header">\r    <p>\r      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\r    </p>\r  </div>\r  <div  class="popup_menu">\r    <ul id="nav">\r      <li>\r        <a href="#">\r          <img width="14" height="14" src="img/host.png"/>\r          [*SQL.cu*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        <a href="../?p=cic_admin_struct">\r          <img width="14" height="14" src="img/cic.png"/>\r          [*SQL.dbs*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        Очистка\r      </li>\r    </ul>\r  </div>\r</div>'),
(103, 'SQL\rSELECT * FROM (SELECT COUNT(id) AS cnid FROM `oba$chunck_name` WHERE `id` NOT IN (SELECT DISTINCT `chunck_name_id` FROM `oba$chunck`)) AS t1 JOIN (SELECT COUNT(id) AS cvid FROM `oba$chunck_val` WHERE `id` NOT IN (SELECT DISTINCT `chunck_val_id` FROM `oba$chunck`)) AS t2\r\rBLOCK\r<div class="section box">\r  <p>Очистить все</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_folder"  method="POST" action="siteman.php?action=purge_all">\r        <input name="gotourl" type="hidden" value="[*ENV.HTTP_REFERER*]">\r        <strong>[*SQL.cnid*]</strong> - имена контейнеров,  <strong>[*SQL.cvid*]</strong> - значения контейнеров будут удалены\r        <button type="submit" style="margin: -4px 0;">  Очистить все  </button>\r      </form>\r    </div>\r  </div>\r</div>'),
(104, 'SQL\rSELECT COUNT(`id`) AS cnt FROM `oba$chunck_name` WHERE `id` NOT IN (SELECT DISTINCT `chunck_name_id` FROM `oba$chunck`)\r\rBLOCK\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_folder"  method="POST" action="siteman.php?action=purge_names">\r        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]">\r        <strong>[*SQL.cnt*]</strong> - свободных имен контейнеров (будут удалены)\r        <button type="submit" style="margin: -4px 0;">  Очистить </button>\r      </form>\r    </div>\r  </div>\r'),
(105, 'SQL\rSELECT COUNT(`id`) AS cnt FROM `oba$chunck_val` WHERE `id` NOT IN (SELECT DISTINCT `chunck_val_id` FROM `oba$chunck`)\r\rBLOCK\r  <div class="box2">\r    <div id="txt_fld11">\r      <form name="del_folder"  method="POST" action="siteman.php?action=purge_values">\r        <input name="gotourl" type="hidden" value="[*ENV.REQUEST_URI*]">\r        <strong>[*SQL.cnt*]</strong> - свободных значений контейнеров (будут удалены)\r        <button type="submit" style="margin: -4px 0;">  Очистить  </button>\r      </form>\r    </div>\r  </div>\r'),
(106, 'Очистка таблиц'),
(107, 'Location: /?p=cic_admin_template\r\r\r[*command*]\r'),
(108, 'SQL\rDELETE FROM `oba$template` WHERE `ID`=0[*GET.id*]\r\rBLOCK\r'),
(109, 'SQL\rSELECT `name`  FROM `oba$template` WHERE `id`=[*GET.id*]\r\rBLOCK\r<div class="section box">\r  <h2>Изменить шаблон</h2>\r  <p>[*SQL.name*]</p>\r  <div class="box2">\r    <div id="txt_fld11">\r      Для редактирование шаблонов используйте <a href="../?p=cic_admin_filemanager"><strong>Файловый менеджер</strong></a>\r    </div>\r  </div>\r</div>'),
(110, '<p id="" class="section box"><a href="../?p=cic_admin_template"> &lt; Назад</a> | <strong> ШАБЛОНЫ</strong></p>'),
(111, 'SQL\rSELECT USER() AS cu, DATABASE() AS dbs\r\rBLOCK\r<div id="banner">\r  <div id="left_header">\r    <p>\r      <a href="../?p=cic_admin_logout">&lt; Выйти</a>\r    </p>\r  </div>\r  <div  class="popup_menu">\r    <ul id="nav">\r      <li>\r        <a href="#">\r          <img width="14" height="14" src="img/host.png"/>\r          [*SQL.cu*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        <a href="../?p=cic_admin_struct">\r          <img width="14" height="14" src="img/cic.png"/>\r          [*SQL.dbs*]\r        </a>&nbsp;&nbsp;&gt; \r      </li>\r      <li>\r        Шаблоны\r      </li>\r    </ul>\r  </div>\r</div>'),
(112, 'SQL\rSELECT `name` FROM `oba$template` WHERE `id`=0[*GET.id*]\r\rBLOCK\rРедактировать шаблон - [*SQL.name*]'),
(114, '<p id="" class="section box"><a href="#"> &lt; Назад</a> | <strong>ГЛАВНАЯ</strong></p>'),
(115, '<!-- jQuery and jQuery UI (REQUIRED) --> \r<link rel="stylesheet" type="text/css" media="screen" href="filemanager/css/jquery-ui.css">\r<script type="text/javascript" src="filemanager/js/jquery.min.js"></script>\r<script type="text/javascript" src="filemanager/js/jquery-ui.min.js"></script>\r<!-- elFinder CSS (REQUIRED) -->		\r<link rel="stylesheet" type="text/css" media="screen" href="filemanager/css/elfinder.min.css">\r<link rel="stylesheet" type="text/css" media="screen" href="filemanager/css/theme.css">\r\r<script type="text/javascript" src="js/colorer.js"></script>\r\r<!-- elFinder JS (REQUIRED) -->\r<script type="text/javascript" src="filemanager/js/elfinder.min.js"></script>\r<!-- elFinder translation (OPTIONAL) -->\r<script type="text/javascript" src="filemanager/js/i18n/elfinder.ru.js"></script>\r<!-- elFinder initialization (REQUIRED) -->\r<script type="text/javascript" charset="utf-8">\r  $().ready(function() {\r    var elf = $(''#elfinder'').elfinder({\r      lang: ''ru'',             // language (OPTIONAL)        \r      url : ''filemanager/php/connector.php''  // connector URL (REQUIRED)          \r    }).elfinder(''instance'');      \r  });    \r</script>');

-- --------------------------------------------------------

--
-- Структура таблицы `oba$config`
--

CREATE TABLE IF NOT EXISTS `oba$config` (
  `key` varchar(64) NOT NULL DEFAULT '' COMMENT 'ключ',
  `value` varchar(255) NOT NULL DEFAULT '' COMMENT 'значение',
  `description` varchar(255) NOT NULL DEFAULT '' COMMENT 'описание',
  `opt1` int(11) DEFAULT NULL COMMENT 'опция 1',
  `opt2` varchar(255) DEFAULT NULL COMMENT 'опция 2',
  UNIQUE KEY `key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Конфигурация';

--
-- Дамп данных таблицы `oba$config`
--

INSERT INTO `oba$config` (`key`, `value`, `description`, `opt1`, `opt2`) VALUES
('admin', '*4ACFE3202A5FF5CF467898FC58AAB1D615029441', 'Логин и пароль администратора(по умолчанию пароль:admin)', 101, ''),
('phpmyadmin_url', 'http://localhost/tools/phpmyadmin', 'Путь к phpMyAdmin', NULL, NULL);

-- --------------------------------------------------------

--
-- Структура таблицы `oba$folder`
--

CREATE TABLE IF NOT EXISTS `oba$folder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(256) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблицы `oba$paget`
--

CREATE TABLE IF NOT EXISTS `oba$paget` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(256) DEFAULT NULL,
  `PARENT_ID` int(11) DEFAULT NULL,
  `TEMPLATE_NAME` varchar(256) DEFAULT NULL,
  `AUTHORIZED` tinyint(1) DEFAULT '0',
  `CONDITION` text NOT NULL,
  `GUESTURL` varchar(256) DEFAULT '',
  `type` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Дамп данных таблицы `oba$paget`
--

INSERT INTO `oba$paget` (`ID`, `NAME`, `PARENT_ID`, `TEMPLATE_NAME`, `AUTHORIZED`, `CONDITION`, `GUESTURL`, `type`) VALUES
(2, 'cic_admin_c_del', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(3, 'cic_admin_c_mod', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(4, 'cic_admin_c_ren', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(5, 'cic_admin_filemanager', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(6, 'cic_admin_f_del', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(7, 'cic_admin_f_ren', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(8, 'cic_admin_home', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(9, 'cic_admin_login', 0, 'admin/template.htm', 0, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(10, 'cic_admin_logout', 0, 'None', 0, '', '', -1),
(11, 'cic_admin_p_del', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(12, 'cic_admin_p_ren', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(13, 'cic_admin_p_mod ', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(14, 'cic_admin_struct', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(15, 'cic_admin_template', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(16, 'cic_admin_tool_change', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(17, 'cic_admin_tool_mysql', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(18, 'cic_admin_tool_purge', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(19, 'cic_admin_t_del', 0, 'None', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1),
(20, 'cic_admin_t_edit', 0, 'admin/template.htm', 1, 'SELECT *  FROM `oba$config` WHERE `key`=''[*COOKIE.admin_name*]'' AND `opt1`=101', '?p=cic_admin_login', -1);

-- --------------------------------------------------------

--
-- Структура таблицы `oba$template`
--

CREATE TABLE IF NOT EXISTS `oba$template` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `NAME` varchar(256) DEFAULT NULL,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп данных таблицы `oba$template`
--

INSERT INTO `oba$template` (`ID`, `NAME`) VALUES
(1, 'None');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
