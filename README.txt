Content In Container - CIC
(version 1.00 beta)

Installation:
1. Create folder "your_site"
2. Extract files to "your_site/www/"
3. In file "your_site/www/admin/config.php" change follows:
	$dbname = 'your_db_name';
	$dbuser = 'your_user_name';
	$dbpasswd = 'your_password';
3. Open "your_db_name" with phpMyAdmin and load "install.sql".

Admin Panel: "http://your_site/admin/"